<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->string('alias');
            $table->string('mime');
            $table->string('original_filename');
            $table->string('description');
            $table->integer('width');
            $table->integer('height');
            $table->boolean('is_deleted');
            $table->boolean('is_original');
            $table->json('resized_copies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files');
    }
}
