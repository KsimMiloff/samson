<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_attributes', function ($table) {
            $table->increments('id');
            $table->text('name');
            $table->text('title');
            $table->text('key');
            $table->integer('position');
            $table->boolean('is_deleted');
//            $table->json('values')->nullable();
//            $table->json('previous_values')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dynamic_attributes');
    }
}
