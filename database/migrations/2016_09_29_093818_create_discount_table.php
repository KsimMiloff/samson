<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('percent');
            $table->string('state');
            $table->text('desc');
            $table->boolean('is_stoped')->nullable();
            $table->timestamps();
        });

        Schema::table('items', function ($table) {
            $table->integer('discount_id')->nullable();
            $table->integer('discounted_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function ($table) {
            $table->dropColumn(['discounted_price']);
            $table->dropColumn(['discount_id']);
        });

        Schema::drop('discounts');
    }
}
