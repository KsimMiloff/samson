<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('price')->nullable();
            $table->string('category_id');
            $table->string('state');
            $table->text('short_desc');
            $table->text('desc');
            $table->string('image_id')->nullable();
            $table->json('image_ids')->nullable();
            $table->json('props')->nullable();
            $table->boolean('is_visible')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
