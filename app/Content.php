<?php

namespace App;

use App\Models\Concerns\LifeCycle;
use App\Models\ExtendedFields\Category\HasXFCategoryMixin;
use App\Models\Concerns\SeoableMixin;
use App\Models\Content\Category;

class Content extends BaseXFModel
{
    const CATEGORY_CLASS = Category::class;

    use LifeCycle {
        LifeCycle::__construct as private __LifeCycleConstruct;
    }

    use HasXFCategoryMixin, SeoableMixin;

    protected $table = 'contents';

    protected $guarded = [];

    protected $casts = [
        'image_id'   => 'integer',
        'props'      => 'array',
    ];


    protected $attributes = [
        'is_visible' => true,
    ];


    protected $rules = array(
        'title'       => 'required',
        'category_id' => 'required|in:page,video,advice',
//        'desc'        => 'required',
    );


    public function __construct($attributes = [])
    {
//        $this->__XFCategory();
        $this->__LifeCycleConstruct();
        parent::__construct($attributes);
    }

    public function xfields()
    {
        return $this->category->fields();
    }

    public function scopeVisible($query) {
        return $query->where('is_visible', true);
    }

    public function scopeForMainPage($query) {
        return $query->where('is_visible', true);
    }


    public function getSeotitleAttribute()
    {
        return $this->title;
    }


    public function slugable() {
        return $this->title; // поле из которого строиться слаг
    }

}
