<?php

namespace App\Models\Item;

use App\Models\ExtendedFields\Field\BaseField;
use App\Models\ExtendedFields\Category\BaseCategory;

Use App\DynamicAttribute;

class Category extends BaseCategory
{
    public static $mattress_sizes = [
        '80x190',
        '80x195',
        '80x200',
        '90x190',
        '90x195',
        '90x200',
        '120x190',
        '120x195',
        '120x200',
        '140x190',
        '140x195',
        '140x200',
        '150x190',
        '150x195',
        '150x200',
        '160x190',
        '160x195',
        'default' => '160x200',
        '180x190',
        '180x195',
        '180x200',
        '200x190',
        '200x195',
        '200x200',
    ];

    public static function hash() {
        return [
            'mattress' => [
                'singular' => ['Матрас'],
                'plural'   => ['Матрасы'],
            ],
            'cover'    => [
                'singular' => ['Наматрасник'],
                'plural'   => ['Наматрасники'],
            ],
            'pillow'   => [
                'singular' => ['Подушка'],
                'plural'   => ['Подушки'],
            ],
//            'hardware' => [
//                'singular' => ['Комплектующие'],
//                'plural'   => ['Комплектующие'],
//            ],
        ];
    }


    public static function config()
    {

        return [
            'mattress' => Category::mattress_config(),

            'cover' => [
                'price_per_square_meter' => BaseField::create([
                    'title'   => 'Цена за квадратный метр',
                    'type'    => 'Integer',
                    'storage' => 'price',
                ]),
            ],
            'pillow' => [
                'price_for_one' => BaseField::create([
                    'title'   => 'Цена',
                    'type'    => 'Integer',
                    'storage' => 'price',
                ]),
            ],
//            'hardware' => [
//                'price_for_one' => BaseField::create([
//                    'title'   => 'Цена',
//                    'type'    => 'Integer',
//                    'storage' => 'price',
//                ]),
//            ],
        ];
    }


    private static function mattress_config()
    {
        $product_lines = DynamicAttribute::options_for( 'product_line' );
        $spring_block  = DynamicAttribute::options_for( 'spring_block' );
        $cover_values  = DynamicAttribute::options_for( 'cover' );

        return [

            'price_per_square_meter' => BaseField::create([
                'title'   => 'Цена за кв. м.',
                'type'    => 'Integer',
                'storage' => 'price',
            ]),
            'product_line' => BaseField::create([
                'title'   => 'Товарная линия',
                'storage' => 'props.product_line',
                'type'    => 'Enum',
                'allowed_values' => $product_lines,

            ]),

            'spring_block' => BaseField::create([
                'title'   => 'Пружинный блок',
                'storage' => 'props.spring_block',
                'type'    => 'Enum',
                'allowed_values' => $spring_block

            ]),


            'cover_1' => BaseField::create([
                'title'   => 'Покрытие (сторона 1)',
                'type'    => 'Options',
                'storage' => 'props.cover_1',
                'allowed_values' => $cover_values,
            ]),

            'cover_2' => BaseField::create([
                'title'   => 'Покрытие (сторона 2)',
                'type'    => 'Options',
                'storage' => 'props.cover_2',
                'allowed_values' => $cover_values,
            ]),

            'upholstery' => BaseField::create([
                'title'   => 'Обивка',
                'storage' => 'props.upholstery',
                'type'    => 'Enum',
                'allowed_values' => [
                    'cloth_foam_rubber' => 'Ткань обыная с поролоном',
                    'cot_foam_rubber' => 'Ткань ХБ с поролоном',
                    'cot_foam_rubber_sintepon' => 'Ткань ХБ с поролоном и синтипоном',
                    'jacquard' => 'Жаккард',
                    'foamed_jacquard' => 'Жаккард с поролоном',
                    'jacquard_with_sintepon' => 'Стретч-Жаккард стеганный на синтипоне',
                    'viscose' => 'Вискоза',
                ],
            ]),

            'side_ppu' => BaseField::create([
                'title'   => 'Боковое ППУ',
                'storage' => 'props.side_ppu',
                'type'    => 'Enum',
                'allowed_values' => [
                    '4sm' => '4 см',
                    '5sm' => '5 см',
                    '8sm' => '8 см',
                ],
            ]),

                'side_upholstery' => BaseField::create([
                'title'   => '3D air system',
                'storage' => 'props.side_upholstery',
                'type'    => 'Boolean',
            ]),

            'spring_block_gain' => BaseField::create([
                'title'   => 'Усиление пружинного блока',
                'storage' => 'props.spring_block_gain',
                'type'    => 'Boolean',
            ]),


            'has_guarantee' => BaseField::create([
                'title'   => '3 года гарантии',
                'storage' => 'props.has_guarantee',
                'type'    => 'Boolean',
                'value'   => true,
            ]),

            'has_o2' => BaseField::create([
                'title'   => 'Дышащие материалы',
                'storage' => 'props.has_o2',
                'type'    => 'Boolean',
                'value'   => true,
            ]),

            'has_antibacterial' => BaseField::create([
                'title'   => 'Антибактериальная обработка',
                'storage' => 'props.has_antibacterial',
                'type'    => 'Boolean',
                'value'   => true,
            ]),

            'has_antiallergic' => BaseField::create([
                'title'   => 'Антиалергенное покрытие',
                'storage' => 'props.has_antiallergic',
                'type'    => 'Boolean',
                'value'   => true,
            ]),
        ];
    }

}
