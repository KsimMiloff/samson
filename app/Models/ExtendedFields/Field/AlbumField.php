<?php

namespace App\Models\ExtendedFields\Field;

use Form;

class AlbumField extends BaseField
{

    public function from_db_format($value) {

        if (is_string($value)) {
            $value = $this->is_json ? json_decode($value) : $value;
        }

        $image_ids = is_array($value) ? $value : [];

        return collect($value)->filter(function ($v) {
            $is_int = (ctype_digit($v) || is_int($v));
            return $is_int;
        })->all();

    }

    public function to_db_format($value) {
        $value = is_array($value) ? $value : [];
        return $this->is_json ? json_encode($value) : $value;
    }

    public function field($field_name, $value)
    {
        return Form::images($field_name, $value, ['multiple' => true]);
    }

    public function widget($options, $class=null)
    {
        $class = 'col-sm-8';
        return parent::widget($options, $class);
    }

}
