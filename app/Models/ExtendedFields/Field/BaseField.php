<?php

namespace App\Models\ExtendedFields\Field;

use Form;

class BaseField
{

    public function __construct($title, $storage, $type, $value, $allowed_values, $options)
    {
        $this->title = $title;
        $this->storage = $storage;
        $this->type = $type;
        $this->value = $value;
        $this->allowed_values = $allowed_values;
        $this->options = $options;

        $this->is_json = array_get($options, 'is_json', false);
    }

    public static function create($options)
    {
        $title = array_pull($options, 'title');
        $storage = array_pull($options, 'storage');
        $type = array_pull($options, 'type');
        $value = array_pull($options, 'value', null);
        $allowed_values = array_pull($options, 'allowed_values', null);

        $field_class = __NAMESPACE__ . '\\' . $type . 'Field';

        return new $field_class($title, $storage, $type, $value, $allowed_values, $options);
    }

    public function from_db_format($value)
    {
        return null;
    }

    public function to_db_format($value)
    {
        return null;
    }

    public function is_int($value)
    {
        return ctype_digit($value) || is_int($value);
    }

    public function label($field_name)
    {
        return Form::label($field_name, $this->title, ['class' => 'control-label col-sm-2']);
    }

    public function field($field_name, $value)
    {
        return Form::text($field_name, $value, ['class' => 'form-control']);
    }

    public function widget($options, $class=null)
    {

        $class = is_null($class) ? 'col-sm-2' : $class;

        $value = array_get($options, 'value', null);
        $model = array_get($options, 'model', null);
        $alias = array_get($options, 'alias');

        $field_name = isset($model) ? $model . "[" . $alias . "]" : $alias;
        $label = $this->label($field_name);
        $field = $this->field($field_name, $value);

        return $this->html($label, $field, $class);
    }

    public function html($label, $field, $class)
    {
        return "
            <div class='form-group'>
                $label
                <div class='{$class}'>$field</div>
            </div>
        ";
    }
}