<?php

namespace App\Models\ExtendedFields\Field;

use Form;

class OptionsField extends EnumField
{

    public function from_db_format($values) {
        if (! is_array($values))
        {
            $values = [];
        }

        $result = [];

        foreach($this->allowed_values as $key => $v)
        {
            if (! array_key_exists( $key, $values)) {
                $values[$key] = false;
            }

            $result[$key] = !!$values[$key];
        }

        return $result;
    }

    public function to_db_format($values) {
        $result = [];
        foreach($this->allowed_values as $key => $v)
        {
            if (! array_key_exists( $key, $values)) {
                $values[$key] = false;
            }
            $result[$key] = !!$values[$key];
        }

        return $result;
    }

    public function field($field_alias, $value=null) {
        $html = "";

        foreach($this->allowed_values as $key => $title)
        {
            $field = Form::boolean($field_alias . "[$key]", $value[$key]);

            $html .= "
                <span class='pull-left checkbox'>
                    <label>
                         $field $title
                     </label>
                     &nbsp;
                     &nbsp;
                     &nbsp;
                </span>
            ";
        }


        return "<div class=''>" . $html . "</div>";
    }

    public function widget($options, $class=null)
    {
        $class = 'col-sm-8';
        return parent::widget($options, $class);
    }

}
