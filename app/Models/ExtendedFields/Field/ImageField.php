<?php

namespace App\Models\ExtendedFields\Field;

use Form;

class ImageField extends BaseField
{

    public function from_db_format($value) {
        $is_int = (ctype_digit($value) || is_int($value));

        return $is_int ? $value : null;
    }

    public function to_db_format($value) {
        $is_int = (ctype_digit($value) || is_int($value));

        if (! $is_int) {
            $value = null;
        }

        return $value;
    }


    public function field($field_name, $value)
    {
        return Form::images($field_name, $value);
    }

}
