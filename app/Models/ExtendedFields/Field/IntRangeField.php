<?php

namespace App\Models\ExtendedFields\Field;

use Form;

class IntRangeField extends BaseField
{

    public function from_db_format($value)
    {
        return is_array($value) ? $value : [null, null];
    }

    public function to_db_format( $value )
    {
        if ( $this->can_be_valid( $value ) ) {

            ! $this->is_int($value['min']) ?: array_except($value, ['min']);
            ! $this->is_int($value['max']) ?: array_except($value, ['max']);

            return $value;
        }

        return null;
    }

    public function can_be_valid($value) {
        if ( is_array( $value ) ) {
            $value = collect( $value );

            if ( $value->has( 'min' ) || $value->has( 'max' ) ) {
                return true;
            }
        }

        return false;
    }

    public function field($field_alias, $value=null) {
        $min  = Form::text($field_alias . '[min]', $value, ['class' => 'form-control', 'placeholder' => 'от', 'step' => "1"]);
        $max  = Form::text($field_alias . '[max]', $value, ['class' => 'form-control', 'placeholder' => 'до', 'step' => "1"]);
        $separator = "<div style='text-align: center;'><label class='control-label'> &mdash; </label></div>";

        $html = '';
        foreach ( [ $min => 3, $separator => 2, $max => 3 ] as $el => $width ) {
            $html .= "<div class='row col-xs-{$width}''>{$el}</div>";
        }

        return $html;
    }

    public function widget($field_alias, $value = null, $class = 'col-xs-5') {
        $value = $value ?: $this->value;
        $label = $this->label($field_alias);
        $field = $this->field($field_alias);

        return "
            <div class='form-group'>
                $label
                <div class='{$class}'>
                    $field
                </div>
            </div>
        ";
    }

}
