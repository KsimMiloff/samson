<?php

namespace App\Models\ExtendedFields\Field;

use Form;

class EnumField extends BaseField
{

    public function from_db_format($value) {
        return $value;
    }

    public function to_db_format($value) {
        if (collect($this->allowed_values)->has($value)) {
            return $value;
        }
        return null;
    }

    public function field($field_alias, $value=null) {
        return Form::select($field_alias, [null => ''] + $this->allowed_values, $value, ['class' => "form-control"] );
    }

}
