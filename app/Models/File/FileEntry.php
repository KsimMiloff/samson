<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Carbon\Carbon;

class FileEntry extends Model
{
    const FILES_DIR   = "uploads/";

    protected $table = 'files';

    protected $fillable = array('mime', 'original_filename', 'filename');

    public static function upload($file)
    {
        $extension = $file->getClientOriginalExtension();
        $entry = new Fileentry([
            'original_filename' => $file->getClientOriginalName(),
            'mime'     => $file->getClientMimeType(),
            'filename' => $file->getFilename() . '.' . $extension,
        ]);

        $entry->is_original = true;
        $entry->alias = Carbon::today()->format('Y/m/d/');

        if ($entry->save()) {
            Storage::disk('local')->put(FileEntry::storage_path($entry), File::get($file));
            return $entry;
        } else {
            return false;
        }
    }

    public function file() {
        return Storage::disk('local')->get(FileEntry::storage_path($this));
    }

    public static function storage_path($entry) {
        return FileEntry::FILES_DIR . $entry->alias . $entry->filename;
    }
}
