<?php

namespace App\Models\File;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageEntry extends FileEntry
{
    const RESIZED_DIR = "uploads/resized/";

    protected $casts = [
        'resized_copies' => 'array',
    ];

    public function resize($width, $height, $crop) {

        $resized_path       = $this->storage_resized_path($this, $width, $height, $crop);
        $is_file_not_exists = ! Storage::exists($resized_path);

        if ($is_file_not_exists) {

            $original = Image::make($this->file());

            // т.к. мы не сохраняем размеры исходного изображения сразу, то это самый удобный момент для того, чтобы исправить это
            $this->width  = $original->width();
            $this->height = $original->height();
            // сохраняем пути ко всем копиям оригинального изображения
            $this->copies = $resized_path;

            if ($crop) {
                $resized = $original->fit($width, $height);
            } else {
                $resized = $original->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }

            $this->save(); // сохраняем модель
            $resized = $resized->encode($this->mime);
            Storage::disk('local')->put($resized_path, $resized); // сохраняем файл

        }

        return Storage::disk('local')->get($resized_path);
    }

    public function getCopiesAttribute($value) {
        $resized_copies = $this->resized_copies ?: [];
        return $resized_copies;
    }

    public function setCopiesAttribute($value)
    {
        $resized_copies = $this->resized_copies ?: [];

        if (is_array($value)) {
            $resized_copies = $value;
        } else {
            array_push($resized_copies, $value);
        }

        $this->resized_copies = array_unique($resized_copies);
    }

    // генерим путь для изменяемой картинки
    public static function storage_resized_path($entry, $width, $height, $crop) {
        $dir  = ImageEntry::RESIZED_DIR;
        $crop = $crop ? 'crop' : '';
        return "{$dir}{$entry->alias}{$crop}_{$width}x{$height}_{$entry->filename}";
    }

}
