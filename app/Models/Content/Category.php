<?php

namespace App\Models\Content;

use App\Models\ExtendedFields\Field\BaseField;
use App\Models\ExtendedFields\Category\BaseCategory;

class Category extends BaseCategory
{

    public static function hash() {
        return [
            'page' => [
                'singular' => ['Страница'],
                'plural'   => ['Страницы'],
            ],
            'video'    => [
                'singular' => ['Видео'],
                'plural'   => ['Видео'],
            ],
            'advice'    => [
                'singular' => ['Полезный совет'],
                'plural'   => ['Полезные советы'],
            ],
        ];
    }


    public static function config()
    {
        return [
            'page'  => [
                'intro' => BaseField::create([
                    'title'   => 'Вступление',
                    'type'    => 'Text',
                    'storage' => 'props.intro',
                ]),

                'is_on_main' => BaseField::create([
                    'title'   => 'Показывать на главной?',
                    'type'    => 'Boolean',
                    'storage' => 'props.is_on_main',
                ]),

                'slider_ids' => BaseField::create([
                    'title'   => 'Слайдер',
                    'storage' => 'props.image_ids',
                    'type'    => 'Album',
                    'is_json' => true,
                ])
            ],

            'video' => [
                'youtube_id' => BaseField::create([
                    'title'   => 'ID на YouTUBE',
                    'type'    => 'String',
                    'storage' => 'props.youtube_id',
                ]),

                'is_on_main' => BaseField::create([
                    'title'   => 'Показывать на главной?',
                    'type'    => 'Boolean',
                    'storage' => 'props.is_on_main',
                ])
            ],
            'advice'  => [
                'is_on_main' => BaseField::create([
                    'title'   => 'Показывать на главной?',
                    'type'    => 'Boolean',
                    'storage' => 'props.is_on_main',
                ]),

                'slider_ids' => BaseField::create([
                    'title'   => 'Слайдер',
                    'storage' => 'props.image_ids',
                    'type'    => 'Album',
                    'is_json' => true,
                ])
            ],
        ];
    }

}
