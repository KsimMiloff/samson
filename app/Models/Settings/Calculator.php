<?php

namespace App\Models\Settings;

use App\Settings;
use App\Models\Item\Category as ItemCategory;


class Calculator extends Settings
{

    public static function read() {
        return static::firstOrCreate(['setname' => 'calculator'])->make();
    }


    public function calculate ($b, $l1, $l2, $m, $d, $g, $with, $height) {

        if ($d) {
            $d = $this->$d;
        }

        $s = $with * $height * 0.0001;
        $p = ($with + $height) * 2;

        $sum_vars_to_square = 0;
        foreach ([$b, $l1, $l2, $m, $g] as $key => $props) {
            // лучше бы рекурси, но времени нет
            foreach ($props as $prop)
            {
                if (is_array($prop)) {
                    foreach ($prop as $prp) {
                        $sum_vars_to_square += $this->$prp;

                    }

                } else {
                    $sum_vars_to_square += $this->$prop;
                }
            }

        }

        $price = ( $sum_vars_to_square * $s + $d * $p ) * $this->k;

        return $price;
    }


    public function variables_list()
    {
        $vlist = Calculator::variables_config();
        $matt_fields = ItemCategory::config()['mattress'];
        foreach ($vlist as $var_name => &$sets)
        {

            $allowed_values = [];

            $values = $this->assign_allowed_values( $matt_fields, $sets['alias'] );

            $allowed_values = array_merge( $allowed_values, $values );
            $alias_values   = $allowed_values;

            if ( isset( $sets['concats'] ))
            {
                foreach ($sets['concats'] as $concated)
                {
                    $concated_values = $this->assign_allowed_values( $matt_fields, $concated['alias'] );
                    $allowed_values = array_merge( $allowed_values, $concated_values );
                }
            } else {
                $sets['concats'] = [];
            }

            $sets['title'] = $matt_fields[$sets['alias']]->title;

            $sets['allowed_values'] = $allowed_values;
            $sets['alias_values'] = $alias_values;

        }

        return $vlist;

    }

    private function assign_allowed_values($matt_fields, $alias)
    {
        $values = [];
        $field = $matt_fields[$alias];

        if ($field->type == 'Enum' || $field->type == 'Options')
        {
            $values = $field->allowed_values;
        } elseif ($field->type == 'Boolean')
        {
            $values = [$alias => $field->title];
        }

        return $values;
    }

    public function groups()
    {
        $vlist  = $this->variables_list();
        $groups = [];
        foreach ($vlist as $vname => $sets)
        {
            $g = array_pull($sets, 'group');

            if (! isset($groups[$g]))
            {
                $groups[$g] = [];
            }

            $groups[$g] = array_merge( $groups[$g], $sets['allowed_values'] );

        }

        $groups['K'] = ['k' => 'Коэффициент маржи'];

        return $groups;
    }


    private function make()
    {
        $fields = collect( $this->groups() )->values()->collapse()->toArray();

        foreach ( $fields as $alias => $title ) {
            $this->associate_props($alias);
        }

        return $this;
    }

    private static function variables_config()
    {
        return [
            'b'  => [
                'group' => 'B',
                'alias' => 'spring_block',
                'multi' => false,
                'required' => true,
            ],

            'b1' => [
                'group' => 'B',
                'alias' => 'spring_block_gain',
                'multi' => false,
                'required' => false
            ],

            'l1' => [
                'group' => 'L',
                'alias' => 'cover_1',
                'multi' => true,
                'required' => true
            ],
            'l2' => [
                'group' => 'L',
                'alias' => 'cover_2',
                'multi' => true,
                'required' => true
            ],
            'm'  => [
                'group' => 'M',
                'alias' => 'upholstery',
                'multi' => false,
                'required' => true
            ],
            'd'  => [
                'group' => 'D',
                'alias' => 'side_ppu',
                'multi' => false,
                'required' => false
            ],
            'g'  => [
                'group' => 'G',
                'alias' => 'side_upholstery',
                'multi' => false,
                'required' => false
            ]
        ];
    }

}
