<?php

namespace App\Models\Settings;
use App\Settings;

class Menu extends Settings
{

    public static function read() {
        return static::firstOrCreate([
            'setname' => 'menu',
        ])->make();
    }

    public function groups()
    {
        return ['left_menu', 'right_menu'];

    }


    private function make()
    {
        foreach ( $this->groups() as $alias )
        {
            $this->associate_props($alias, []);
        }

        return $this;
    }

}
