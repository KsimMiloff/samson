<?php

namespace App\Models\Concerns;

use App\Seotag;
use DB;


trait SeoableMixin
{

    public function seotags()
    {
        return $this->morphOne('App\Seotag', 'seoable');
    }


    public function scopeBySlug($query, $slug) // bySlug
    {
        $possible_id = explode("-", $slug)[0];
        return $query->where('slug', $slug)
            ->orWhere('id', $slug)
            ->orWhere('id', $possible_id);
    }


    public function slug_placeholder()
    {
        if ($this->slug) {
//            print_r($this->slug);
//            exit;
            return $this->slug;
        }

        return $this->generate_slug();
    }


    public function getSeotagsAttribute()
    {
        $new_seotags = new Seotag();
        $new_seotags->seoable()->associate($this);
        $seotags = $this->seotags()->first() ?: $new_seotags;
        return $seotags;
    }

    public function setSeotagsAttribute($value)
    {
        $seotags = $this->seotags;
        if (is_array($value)) {
            $seotags->fill($value);
        } else {
            $seotags = $value;
        }
        $seotags->seoable()->associate($this);

        if ($seotags->seoable->exists) {
            // мы можем присваивать сеотеги только уже существующим объектам
            $seotags->save();
        }
    }



    public function getSeoIdAttribute() //seo_id
    {
        if ($this->slug) {
            return $this->slug;
        }
        return $this->id . '-' . str_slug($this->slugable());
    }


    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = $this->generate_slug($value);
    }



    private function slug_is_exists($slug) {
        $matches_count = DB::table($this->table)
            ->where('slug', '=', $slug)
            ->where('id', '<>', $this->id)
            ->count();
        return $matches_count;
    }

    private function generate_slug($slug=null, $i=0)
    {

        if (! $this->exists )
        {
            return null;
        }
//
//        print_r($slug);
//        exit;

        $slug = $slug ?: str_slug( $this->slugable() );


        if ($this->slug_is_exists($slug) )
        {

            if ($i) // если мы уже добавляли $i в конец слага
            {
                $slug_chunks = explode("-", $slug); // разбиваем слаг на массив
                end($slug_chunks); // устанавливаем указатель на последний элемент, т.е. на наш прошлый $i
                $slug_chunks[key($slug_chunks)] = $i + 1; // обновляем последний эелемент, т.е. наращиваем $i
                $slug = implode("-", $slug_chunks); // собираем массив обратно в слаг
            } else {
                $slug = "{$slug}-1";
            }

            $i++;

            return $this->generate_slug($slug, $i);
        } else {
            return $slug;
        }
    }
}