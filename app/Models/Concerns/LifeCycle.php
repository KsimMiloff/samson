<?php

namespace App\Models\Concerns;

use App\Models\Concerns\DynamicMethods;
use Illuminate\Support\Str;

trait LifeCycle
{
    protected static $valid_life_states = ['live', 'archive'];


    public function __construct()
    {
        // устанавливаем значение по умолчанию, отработает только для нового объекта
        $this->attributes['state'] = 'live';

        foreach ( static::$valid_life_states as $state ) {
            $scope  = Str::camel('scope_' . $state);
            $getter = 'get' . Str::studly("is_{$state}") . 'Attribute';

            $this->addStaticMethod($state, function ($query) use ($state) {
                return $query->where('state', $state);
            });

            $this->addMethod($getter, function () use ($state) {
                return $this->state == $state;
            });

        }
    }

    public function scopeByState($query, $state) {
        return $query->where('state', $state);
    }

    public function move_to($state)
    {
        if (collect (static::$valid_life_states)->contains($state))
        {
            $this->state = $state;
            return $this->save(['validate' => false]);
        }

        $this->errors = ['storage' => 'Wrong storage name'];
        return false;
    }

    public function move_to_archive() {
        return $this->move_to('archive');
    }

    public function move_to_live() {
        return $this->move_to('live');
    }
}