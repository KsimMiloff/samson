<?php

namespace App\Models\Concerns\DynamicMethods;

use Illuminate\Support\Str;

trait Mutators
{
    // пришлось перенести сюда, т.к. чаще всего addMethods используется для объявление геттер/сеттер-методов,
    // а для включения их в область видимости Eloquent, нужно переопределить два след. метода.
    public function hasGetMutator($key)
    {
        // переопределяем родительский поиск мутаторов
        // учим их находить динамически определенные методы
        $getter = $this->getter_mutator($key);
        return method_exists($this, $getter) || isset($this->methods[$getter]);
    }

    // тоже, что и в предыдущем методе
    public function hasSetMutator($key)
    {
        $setter = $this->setter_mutator($key);
        return method_exists($this, $setter) || isset($this->methods[$setter]);
    }

    public function getter_mutator($alias) {
        return 'get' . Str::studly($alias) . 'Attribute';
    }


    public function setter_mutator($alias) {
        return 'set' . Str::studly($alias) . 'Attribute';
    }


}