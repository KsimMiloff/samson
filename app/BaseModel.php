<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class BaseModel extends Model
{
    protected $rules = [];
    protected $attributeNames = [];
    protected $errors;

    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);
        $v->setAttributeNames($this->attributeNames);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->errors()->all();
            return false;
        }

        // validation pass
        return true;
    }

    public function save(array $options = [], $validate = true)
    {
        if (in_array( 'validate', $options )) {
            $validate = array_get($options, 'validate');
            $options  = array_except($options, 'validate');
        }

        if ($validate && $this->validate($this->attributes)) {
            return parent::save($options);
        } else {
            return false;
        }
    }

    public function getErrorsAttribute()
    {
        return $this->errors;
    }

    // оставил для совместимости
    public function errors()
    {
        return $this->errors;
    }


}
