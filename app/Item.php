<?php

namespace App;

use App\Models\ExtendedFields\Field\BaseField;
use App\Models\Concerns\LifeCycle;
use App\Models\Concerns\SeoableMixin;
use App\Models\ExtendedFields\Category\HasXFCategoryMixin;
use App\Models\Item\Category;

class Item extends BaseXFModel
{
    const CATEGORY_CLASS = Category::class;

    use LifeCycle {
        LifeCycle::__construct as private __LifeCycleConstruct;
    }

    use HasXFCategoryMixin, SeoableMixin;


    protected $table = 'items';

    protected $guarded = [];

    protected $casts = [
        'image_id'    => 'integer',
        'image_ids'   => 'array',
        'props'       => 'array',
        'seted_ids'   => 'array',
    ];


    protected $attributes = [
        'is_visible' => true
    ];


    protected $rules = array(
        'name' => 'required',
        'category_id' => 'required|in:mattress,cover,pillow,hardware',
    );


    public function discount()
    {
        return $this->belongsTo('\App\Discount');
    }


    public function __construct($attributes = [])
    {
//        $this->__XFCategory();
        $this->__LifeCycleConstruct();
        parent::__construct($attributes);
    }


    public function xfields() {
        $xfields = $this->category->fields();
//        $xfields = $this->set_dynamic_attributes();

        return $xfields + [
//            'image_id' => BaseField::create([
//                'title'   => 'Изображение',
//                'storage' => 'image_id',
//                'type'    => 'Image'
//            ]),

//            'slider_ids' => BaseField::create([
//                'title'   => 'Слайдер',
//                'storage' => 'image_ids',
//                'type'    => 'Album',
//                'is_json' => true,
//            ]),
        ];
    }


    public function getPriceAttribute()
    {

        return $this->price('real');
    }


    public function price( $type )
    {
        if ($this->category_id == 'mattress' || $this->category_id == 'cover') {
            return $this->sized_price( ['type' => $type] );
        }

        return array_get($this->attributes, 'price', null);
    }


    public function sized_price($options)
    {

        $size = array_get($options, 'size', Category::$mattress_sizes['default']);

        list($w, $h) = explode('x', $size);

        $w = array_get($options, 'w', $w);
        $h = array_get($options, 'h', $h);


        switch ( array_get($options, 'type', 'cart') ) {
            case 'cart':
                $price_per_square_meter = $this->price_per_square_meter;
                if (! $this->has_discount() ) { break; }

            case 'discount':
                $price_per_square_meter = $this->price_per_square_meter( 'discount' );
                break;

            default:
                $price_per_square_meter = $this->price_per_square_meter;
        }

        return $price_per_square_meter * $w * $h * 0.0001;
    }


    public function price_per_square_meter( $type='real' ) {

        if ($type == 'discount' && $this->has_discount())
        {
            $decrease_to = $this->price_per_square_meter * $this->discount->factor();
            return $this->price_per_square_meter - $decrease_to;
        }
        return $this->price_per_square_meter;
    }



    public function has_discount() {
        if (count($this->discount)) {
            return true;
        }
        return false;
    }


    public function getSeotitleAttribute() // seotitle
    {
        return $this->category->title . ' ' . $this->name;
    }



    public function getSetedItemsAttribute() // seted_items
    {
        return static::byState('live')->whereIn('id', $this->seted_ids)->get();
    }


    public function in_my_set($id)
    {
        if (is_array($this->seted_ids)) {
            return in_array($id, $this->seted_ids);
        }
        return false;
    }



    public function slugable() {
        return $this->name; // поле из которого строиться слаг
    }


//    // По требованию заказчика мы переходим на динамические характеристики
//    // гавно-метод для переопределения объявленных по старинке характеристик
//    private function set_dynamic_attributes() {
//        $xfields = $this->category->fields();
//
//        $product_lines = DynamicAttribute::where([ 'name' => 'product_line' ])->first()->values;
//
//        return $xfields + [
//            'product_line' => BaseField::create([
//                'title'   => 'Товарная линия',
//                'storage' => 'props.product_line',
//                'type'    => 'Enum',
//                'allowed_values' => $product_lines
////                [
////                    'bs'   => 'BS',
////                    'ps'   => 'PS',
////                    'ws'   => 'WS',
////                    'mp'   => 'MP',
////                    'ep'   => 'Evo Premium',
////                    'b'   =>  'Дети',
////                ],
//            ]),
//        ];
//    }

}
