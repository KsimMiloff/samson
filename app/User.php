<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\SoftDeletes;


class User extends BaseModel implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['deleted_at'];

    protected $rules = [
        'email' => 'required|email',
        'password' => 'required',
        'name'  => 'required'
    ];


    public function validate($data) {
        $this->rules['email'] .= '|unique:users,email,'.$this->id; // исключаем из проверки на уникальность email текущего пользователя
        return parent::validate($data);
    }


    public function set_password($pass) {
        $this->password = bcrypt($pass);
//        \Auth::logout();
    }

    public function nullize_password() {
        $this->password = null;
    }

    public function trashed_as_new() {
        $this->deleted_at = null;
        $this->is_admin = false;
        $this->is_developer = false;
        $this->nullize_password();
    }
}
