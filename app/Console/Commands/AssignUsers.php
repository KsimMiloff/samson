<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

class AssignUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:assign {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User();
        $user->email    = $this->argument('email');
        $user->password = bcrypt($this->argument('password'));

        if ( empty( $user->name ))
        {
            $user->name = 'Site';
        }
        $user->save();

//        print_r($user);
//        print_r('\n');
    }

}
