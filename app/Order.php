<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Item\Category;

class Order extends Model
{

    public static $states = [
        'not_viewed' => 'Не просмотрен',
        'viewed'     => 'Просмотрен',
        'approved'   => 'Принят',
        'rejected'   => 'Отклонен',
    ];

    protected $table = 'orders';

    protected $guarded = [];

    protected $casts = [
        'cart' => 'array',
    ];

    protected $attributes = [
        'state' => 'not_viewed',
    ];

    public function getStateTitleAttribute()
    {
        return Order::$states[$this->state];
    }

    public function getCartItemsAttribute()
    {

        $cart = collect ($this->cart)->map(function($item, $key) {
            $item['category'] = Category::find($item['category_id']);

            return $item;
        })->toArray();

        return $cart;
    }

}
