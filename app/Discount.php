<?php

namespace App;


class Discount extends BaseModel
{

    protected $table = 'discounts';

    protected $guarded = [];

//    protected $casts = [
//        'props'       => 'array',
//    ];


    protected $attributes = [
        'is_stoped' => false
    ];


    protected $rules = array(
        'title' => 'required',
    );


    public function scopeLive($query) {
        return $query;//->where('state', $state);
    }


    public function items()
    {
        return $this->hasMany('App\Item');
    }


    public function reassign_items()
    {
        foreach( $this->items()->get() as $item )
        {
            $item->discount_id = null;
            $item->save(['validate' => false]);
        }
    }

    public function factor() {
        return $this->percent / 100;
    }

}
