<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Models\Item\Category as ItemCategory;
use App\Models\Content\Category as ContentCategory;


Route::get('/auth/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('/auth/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('/auth/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@getLogout']);


Route::controllers([
    'password' => 'Auth\PasswordController',
]);


Route::group(['namespace' => 'File'], function()
{
    Route::post('files/wisiwyg_upload}',
        ['as' => 'files.wisiwyg_upload', 'uses' => 'FilesController@wisiwyg_upload']
    );

    Route::get('images/{id}/resized/{size}/{crop?}',
        ['as' => 'images.resized', 'uses' => 'ImagesController@resize']
    );
    Route::resource('images', 'ImagesController', ['except' => ['destroy']]);
    Route::resource('files', 'FilesController', ['except' => ['destroy']]);
});


// Admin area
Route::get('admin', function () {
    return redirect('/admin/items');
});


Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function()
{
    Route::get('items/create/{category_id?}', ['as' => 'admin.items.create', 'uses' => 'ItemsController@create']);
    Route::resource('items', 'ItemsController', ['except' => ['create']]);

    Route::get('contents/create/{category_id?}', ['as' => 'admin.contents.create', 'uses' => 'ContentsController@create']);
    Route::resource('contents', 'ContentsController', ['except' => ['create']]);

    Route::resource('discounts', 'DiscountsController');

    Route::resource('calculator', 'CalculatorController', ['only' => ['index', 'update']]);
    Route::resource('menu', 'MenuController', ['only' => ['index', 'update']]);

    Route::resource('orders', 'OrderController', ['only' => ['index', 'edit', 'update']]);

    Route::resource('users', 'UsersController', ['except' => ['show']]);

    Route::resource('dynamic_attributes', 'DynamicAttributesController', ['only' => ['index', 'edit']]);
    Route::post('dynamic_attributes/save', ['as' => 'admin.dynamic_attributes.save', 'uses' => 'DynamicAttributesController@save']);

});



Route::group(['middleware' => ['cart', 'seotags']], function () {

    Route::get('/', 'WelcomeController@index');


    Route::get('items/{category_id}', [
        'as' => 'items.index',
        'uses' => 'ItemsController@index'
    ])
        ->where(['category_id' => collect(ItemCategory::ids())->implode('|')]);

    Route::resource('items', 'ItemsController', ['only' => ['show']]);


    Route::get('{category_id}', [
        'as' => 'contents.index',
        'uses' => 'ContentsController@index'
    ])
    ->where(['category_id' => collect(ContentCategory::ids())->implode('|')]);

    Route::get('{category_id}/{id}', [
        'as' => 'contents.show',
        'uses' => 'ContentsController@show'
    ])
    ->where(['category_id' => collect(ContentCategory::ids())->implode('|')]);


    Route::get('calculator', ['as' => 'calculator', 'uses' => 'CalculatorController@index']);

});

Route::resource('cart', 'CartController', ['except' => ['store']]);
Route::post('cart/buy', ['as' => 'cart.buy', 'uses' => 'CartController@buy']);
Route::post('cart/{item_id}', ['as' => 'cart.store', 'uses' => 'CartController@store']);
Route::get('cart/refresh_status', ['as' => 'cart.refresh_status', 'uses' => 'CartController@refresh_status']);



