<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Item;
use App\Discount;
use App\Seotag;
use App\Models\Item\Category;

class ItemsController extends Controller
{

    public function __construct(Request $request)
    {

        if ($request->route('items')) {
            $this->item = Item::find($request->route('items'));
        }

        if ($request->route('category_id')) {
            $this->category = Category::find( $request->route( 'category_id' ) );

            if ( is_null( $this->category ) ) {
                abort(404);
            }
        }

        $category_id = $request->get('category_id');

        if (! is_null( $category_id )) {
            $request->session()->put('item.category_id', $category_id);
        }

        $this->middleware('App\Http\Middleware\ItemsMiddleware');
        $this->request = $request;

    }


    public function index(Request $request)
    {
        $state       = $request->get('state', 'live');
        $category_id = $request->session()->get('item.category_id');


        $items = Item::byState($state);
        if ($category_id) {
            $items      = $items->byCategoryId($category_id);
        }

        return view('admin.items.index', [
            'items'       => $items->orderBy('created_at', 'desc')->get(),
            'categories'  => Category::all(),
            'category_id' => $category_id,
            'state'       => $state
        ]);
    }


    public function create()
    {
        $item = new Item([
            'category_id' => $this->category->id,
        ]);

        $related_items = $this->related_items( $item );

        return view('admin.items.new', [
            'item' => $item,
            'crumbs' => $this->crumbs($item),
            'category_siblings' => $related_items['category_siblings'],
            'items_for_set' => $related_items['items_for_set'],
        ]);
    }


    public function store(Request $request)
    {
        $item = new Item($this->item_params());

        if ( $item->save() ) {
            flash()->success('Товар создан');

            return redirect()->route(
                $request->has('action.save_and_new') ? 'admin.items.create' : 'admin.items.index',
                ['category_id' => $item->category_id]
            );

        } else {
            flash()->error('Товар не был создан!');

            $related_items = $this->related_items( $item );

            return view('admin.items.new', [
                'item'   => $item,
                'crumbs' => $this->crumbs($item),
                'category_siblings' => $related_items['category_siblings'],
                'items_for_set' => $related_items['items_for_set'],
            ]);
        }

    }

    public function edit($id)
    {
//var_dump($this->item->seted_ids);
//        exit;

        $related_items = $this->related_items( $this->item );

        return view('admin.items.edit', [
            'item'   => $this->item,
            'crumbs' => $this->crumbs($this->item),
            'category_siblings' => $related_items['category_siblings'],
            'items_for_set' => $related_items['items_for_set'],
            'discounts' => $this->discounts(),
        ]);
    }


    public function update(Request $request, $id)
    {

//        $this->make_discount();

        $this->item->fill($this->item_params());

        if ( $request->has('action.move_to_archive') ) {
            $save    = 'move_to_archive';
            $success_msg = 'Товар перемещен в архив';
            $error_msg   = 'Заархивировать товар не удалось!';
        }

        elseif ( $request->has('action.move_to_live') ) {
            $save    = 'move_to_live';
            $success_msg = 'Товар восстановлен из архива';
            $error_msg   = 'Восстановить товар не удалось!';
        }

        else {
            $save = 'save';
            $success_msg = 'Товар сохранен';
            $error_msg   = 'Товар не был сохранен!';
        }


        if ( $this->item->$save() ) {
            flash()->success($success_msg);
        } else {
            flash()->error($error_msg);

            return view('admin.items.edit', [
                'item'    => $this->item,
                'crumbs'  => $this->crumbs($this->item)
            ]);
        }

        return redirect()->route(
            "admin.items.{$request->get('redirect_to', 'edit')}",
            ['id' => $this->item]);

    }


    private function item_params()
    {
        $params  = $this->request->item ?: [];

        // если скидка выбрана не из селекта, а введена вручную, то пытаемся по введеным данным найти существующую такую
        if ( $this->get_discount() )
        {
            $params['discount_id'] = $this->get_discount()->id;
        }

        return $params;
    }


    private function discont_params()
    {
        return request()->discount ?: [];
    }


    private function make_discount()
    {
        $discont_params = $this->discont_params();
        if ( empty( $discont_params ))
        {
            return null;
        }

        $discount = new Discount( $discont_params );
        $discount->save(['validate' => false]);
        return $discount;
    }


    private function get_discount()
    {
        $params   = $this->discont_params();
        $discount = null;

        if ( ! empty( $params["percent"] )
            && ! empty( $params["title"] ) )
        {
            $discount =  Discount::where([
                'percent' => $params['percent'],
                'title'   => $params['title']
            ])->first();

//            print_r($discount);
//            exit;

            if (empty ( $discount ) )
            {
                $discount = $this->make_discount();
            }

        }

        return $discount;
    }


    private function discounts()
    {
        $discounts = Discount::live()
            ->orderBy('created_at', 'desc')
            ->get()
            ->toArray();

        $options = [];

        foreach( $discounts as $d )
        {
            $options[$d['id']] = $d['title'] . ' - ' . $d['percent'] . '%';
        }

        return $options;
    }


    private function related_items($item)
    {
        $category_siblings = $item->category->siblings();
        $items_for_set = Item::live()
            ->whereIn('category_id', $category_siblings->keys())
            ->get()
            ->groupBy('category_id');

        return [
            'category_siblings' => $category_siblings,
            'items_for_set' => $items_for_set
        ];

    }


    private function crumbs($item)
    {
        $crumbs = [
            ['title' => 'Список товаров', 'url' => route('admin.items.index')],
            ['title' => "Категория &laquo;{$item->category->title}&raquo;", 'url' => route('admin.items.index')]
        ];

        if ( $item->exists ) {
            $crumb = ['title' => "Редактривание товара", 'active' => true];
        } else {
            $crumb = ['title' => "Добавление товара", 'active' => true];
        }

        array_push($crumbs, $crumb);

        return collect($crumbs);
    }
}
