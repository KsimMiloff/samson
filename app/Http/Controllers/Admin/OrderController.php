<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;


class OrderController extends Controller
{

    public function index()
    {

        $orders = Order::orderBy('created_at', 'desc');

        return view('admin.orders.index', [
            'orders' => $orders->get(),
        ]);
    }


    public function edit($id)
    {
        $order = Order::find($id);
        $order->state = $order->state == 'not_viewed' ? 'viewed' : $order->state;

        return view('admin.orders.edit', [
            'order' => $order,
            'states' => collect(Order::$states)->except('not_viewed')->toArray(),
            'crumbs' => $this->crumbs()
        ]);
    }


    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->state = $request->order['state'];

        $order->save();

        return redirect()->route(
            "admin.orders.index"
        );

    }



    private function crumbs()
    {
        $crumbs = [
            ['title' => 'Список заказов', 'url' => route('admin.orders.index')],
            ['title' => "Просмотр заказа", 'active' => true]
        ];

        return collect($crumbs);
    }
}
