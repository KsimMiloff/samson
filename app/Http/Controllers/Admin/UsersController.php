<?php

namespace App\Http\Controllers\Admin;

use Gate;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;


class UsersController extends Controller
{


    public function index()
    {
//        $users = User::all();
        $users = User::where(['is_developer' => false])->get();


        return view('admin.users.index', [
            'users' => $users,
        ]);
    }

    public function create()
    {
        $user = new User();

        return view('admin.users.new', [
            'user' => $user,
            'crumbs' => $this->crumbs($user)
        ]);
    }


    public function store(Request $request)
    {
        $user = User::withTrashed()
            ->firstOrNew(['email' => $request->get('email')]);

        if ($user->trashed())
        {
            $user->trashed_as_new();
        }

        $user->fill($this->user_params($request));


        if ( Gate::allows('create-user', $user) )
        {

            $this->assign_unsafe_fields( $user, $request );

            if ( $user->save() ) {
                flash()->success('Пользователь создан');

                return redirect()->route('admin.users.index');

            } else {
                $user->exists = false; // чтобы отображалось поле email

                flash()->error('Пользователь не был создан!');
                return view('admin.users.new', [
                    'user'   => $user,
                    'crumbs' => $this->crumbs($user)
                ]);
            }
        }
        else {
            flash()->error('Нет прав для добавления пользователей');
        }

        return redirect()->route(
            "admin.users.index",
            ['id' => $user]
        );
    }


    public function edit($id)
    {
        $user = $this->find_user( $id );

        return view('admin.users.edit', [
            'user' => $user,
            'crumbs' => $this->crumbs($user)
        ]);
    }


    public function update(Request $request, $id)
    {
        $user = $this->find_user( $id );

        if ( Gate::allows('update-user', $user) )
        {

            $user->fill($this->user_params($request));
            $this->assign_unsafe_fields( $user, $request );

            if ( $user->save() ) {
                flash()->success('Пользователь сохранен');
            } else {
                flash()->error('Сохранить пользователя не удалось');

                return view('admin.users.edit', [
                    'user'    => $user,
                    'crumbs'  => $this->crumbs($user)
                ]);
            }

        } else {
            flash()->error('Нет прав для изменения этого пользователя');
        }

        return redirect()->route(
            "admin.users.index",
            ['id' => $user]);
    }

    public function destroy($id)
    {
        $user = $this->find_user( $id );

        if ( Gate::allows('update-user', $user) ) {
            if ($user->delete()) {
                flash()->success('Пользователь удален');
                return redirect()->route("admin.users.index");
            }
        } else {
            flash()->error('Нет прав для удаления пользователя');
        }
    }

    private function user_params($request)
    {
        return $request->user ?: [];
    }

    private function crumbs($user)
    {
        $crumbs = [
            ['title' => 'Список пользователей', 'url' => route('admin.users.index')],
        ];

        if ( $user->exists ) {
            $crumb = ['title' => "Редактривание пользователя {$user->email}", 'active' => true];
        } else {
            $crumb = ['title' => "Добавление нового пользователя", 'active' => true];
        }

        array_push($crumbs, $crumb);

        return collect($crumbs);
    }


    private function find_user($id) {
        $user = User::find($id);

        if ($user->is_developer && !\Auth::user()->is_developer)
        {
            abort(404);
        }

        return $user;
    }


    private function assign_unsafe_fields($user, $request) {
        if ($request->has('password')) {
            $user->set_password($request->get('password'));
        }

        if (Gate::allows('toggle-admin-state')) { // давать/забирать права админа может только другой админ
            $user->is_admin = $request->get('is_admin');
        }
    }
}
