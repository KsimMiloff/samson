<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Settings\Calculator;


class CalculatorController extends Controller
{

    public function index()
    {
        $calculator = Calculator::read();

        return view('admin.calculator.index', [
            'calculator' => $calculator,
            'tab' => $this->request->tab,
        ]);
    }

    public function update(Request $request, $id)
    {
        $calculator = Calculator::read()->fill($this->calculator_params());

        if ( $calculator->save() ) {
            flash()->success('Настройки сохранены');

            return redirect()->route(
                'admin.calculator.index',
                ['tab' => $this->request->tab]
            );

        } else {
            flash()->error('Настройки сохранить не удалось');

            return view('admin.calculator.index', [
                'calculator' => $calculator,
                'tab' => $this->request->tab
            ]);
        }

    }

    private function calculator_params() {
        return $this->request->calculator;
    }
}
