<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Content;
use App\Models\Content\Category;

class ContentsController extends Controller
{
    public function __construct(Request $request)
    {

        if ($request->route('contents')) {
            $this->content = Content::find($request->route('contents'));
        }

        if ($request->route('category_id')) {
            $this->category = Category::find( $request->route( 'category_id' ) );

            if ( is_null( $this->category ) ) {
                abort(404);
            }
        }

        $category_id = $request->get('category_id');

        if (! is_null( $category_id )) {
            $request->session()->put('content.category_id', $category_id);
        }

        $this->request = $request;

    }


    public function index(Request $request)
    {
        $state       = $request->get('state', 'live');
        $category_id = $request->session()->get('content.category_id');

        $contents = Content::byState($state);

        return view('admin.contents.index', [
            'contents'    => $contents->orderBy('created_at', 'desc')->get(),
            'categories'  => Category::all(),
            'category_id' => $category_id,
            'state'       => $state
        ]);
    }

    public function create()
    {
        $content = new content([
            'category_id' => $this->category->id,
        ]);

        return view('admin.contents.new', [
            'content' => $content,
            'crumbs'  => $this->crumbs($content)
        ]);
    }


    public function store(Request $request)
    {
        $content = new content($this->content_params());

        if ( $content->save() ) {
            flash()->success('Контент создан');

            return redirect()->route(
                $request->has('action.save_and_new') ? 'admin.contents.create' : 'admin.contents.index',
                ['category_id' => $content->category_id]
            );


        } else {
            flash()->error('Контент не был создан!');
            return view('admin.contents.new', [
                'content' => $content,
                'crumbs'  => $this->crumbs($content)
            ]);
        }

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {

        return view('admin.contents.edit', [
            'content' => $this->content,
            'crumbs' => $this->crumbs($this->content)
        ]);
    }


    public function update(Request $request, $id)
    {
        $this->content->fill($this->content_params());

        if ( $request->has('action.move_to_archive') ) {
            $save    = 'move_to_archive';
            $success_msg = 'Контент перемещен в архив';
            $error_msg   = 'Заархивировать контент не удалось!';
        }

        elseif ( $request->has('action.move_to_live') ) {
            $save    = 'move_to_live';
            $success_msg = 'Контент восстановлен из архива';
            $error_msg   = 'Восстановить контент не удалось!';
        }

        else {
            $save = 'save';
            $success_msg = 'Контент сохранен';
            $error_msg   = 'Контент не был сохранен!';
        }


        if ( $this->content->$save() ) {
            flash()->success($success_msg);
        } else {
            flash()->error($error_msg);

            return view('admin.contents.edit', [
                'content' => $this->content,
                'crumbs'  => $this->crumbs($this->content)
            ]);
        }

        return redirect()->route(
            "admin.contents.{$request->get('redirect_to', 'edit')}",
            ['id' => $this->content]
        );

    }


    private function content_params()
    {
        return $this->request->content ?: [];
    }

    private function crumbs($content)
    {
        $crumbs = [
            ['title' => 'Список контента', 'url' => route('admin.contents.index')],
            ['title' => "Категория &laquo;{$content->category->title}&raquo;", 'url' => route('admin.contents.index')]
        ];

        if ( $content->exists ) {
            $crumb = ['title' => "Редактривание контента", 'active' => true];
        } else {
            $crumb = ['title' => "Добавление контента", 'active' => true];
        }

        array_push($crumbs, $crumb);

        return collect($crumbs);
    }
}
