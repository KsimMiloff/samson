<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DynamicAttribute;


class DynamicAttributesController extends Controller
{

    public function index(Request $request)
    {
        return view('admin.dynamic_attributes.index', [
            'attributes' => DynamicAttribute::$passible_attrs,
        ]);
    }

    public function save(Request $request)
    {
        $attr_params = $this->attribute_params();
        $name   = $attr_params['name'];
        $values = $attr_params['titles'];

        if ( DynamicAttribute::save_attribute_values( $name, $values ) )
        {
            flash()->success( trans( 'Значения сохранены' ) );
            return redirect()->route( 'admin.dynamic_attributes.index');
        }

        flash()->error( trans( 'Не все значения удалось сохранить' ) );
        return view('admin.dynamic_attributes.edit', [
//            'attribute'  => $attribute,
            'attributes' => $this->attributes,
            ]
        );

    }

    public function edit(Request $request, $name)
    {
        $attributes = DynamicAttribute::where([
            'name' => $name,
            'is_deleted' => 0   ]
        )
            ->orderBy( 'position' )
            ->get();

        return view('admin.dynamic_attributes.edit', [
            'attributes' => $attributes,
            'attr_name' => $name
        ]);
    }


    private function attribute_params()
    {
        $params  = request()->attribute ?: [];
        return $params;
    }



}
