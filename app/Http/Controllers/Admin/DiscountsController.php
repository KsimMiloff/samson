<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Discount;
use App\Item;


class DiscountsController extends Controller
{

    public function index(Request $request)
    {
        $discounts = Discount::live()
            ->orderBy('created_at')
            ->get();

        return view('admin.discounts.index', [
            'discounts' => $discounts,
        ]);
    }

    public function create()
    {
        $discount = new Discount();

        return view('admin.discounts.new', [
            'discount' => $discount,
            'items' => $this->items()
        ]);
    }

    public function store(Request $request)
    {
        $discount = new Discount( $this->discount_params() );

        if ( $this->save($discount) ) {
            flash()->success( trans( 'Cкидка создана' ) );
            return redirect()->route( 'admin.discounts.index');

        }

        flash()->error( trans( 'Скидку сохранить не удалось' ) );
        return view('admin.discounts.edit', [
            'discount' => $discount,
            'items' => $this->items()
            ]
        );

    }

    public function edit(Request $request, $id)
    {
        $discount = Discount::find($id);
        return view('admin.discounts.edit', [
            'discount' => $discount,
            'items' => $this->items(),
            'groups' => \App\DynamicAttribute::options_for('spring_block')
        ]);
    }


    public function update(Request $request, $id)
    {
        $discount = Discount::find($id)
            ->fill( $this->discount_params() );

        if ( $this->save($discount) ) {
            flash()->success( trans( 'Cкидка создана' ) );
            return redirect()->route( 'admin.discounts.index');

        }
        flash()->error( trans( 'Скидку сохранить не удалось' ) );
        return view('admin.discounts.edit', ['discount' => $discount]);

    }


    private function discount_params()
    {
        $params  = request()->discount ?: [];
        unset( $params[ 'item_ids' ] );
        return $params;
    }


    private function save($discount)
    {
        $discount->reassign_items();
        return $discount->save() && $discount->items()->saveMany($this->selected_items());
    }


    private function selected_items()
    {
        $item_ids = request()->discount['item_ids'];
        return Item::live()
            ->whereIn( 'id', $item_ids )
            ->get();
    }


    private function items() {
        return Item::live()
            ->byCategoryId( 'mattress' )
            ->orderBy('created_at', 'desc')
            ->get();
    }
}
