<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Settings\Menu;
use App\Item;
use App\Content;
use App\Models\Item\Category as ItemCategory;
use App\Models\Content\Category as ContentCategory;


class MenuController extends Controller
{

    public function index()
    {
        $menu = Menu::read();


        return view('admin.menu.index', [
            'menu'         => $menu,
            'tabs'         => $this->tabs(),
            'tab_contents' => $this->tab_contents(),
        ]);
    }

    public function update(Request $request, $id)
    {

        $menu = Menu::read()->fill($this->menu_params());

        if ( $menu->save() ) {
            flash()->success('Настройки сохранены');

            return redirect()->route(
                'admin.menu.index',
                ['tab' => $this->request->tab]
            );

        } else {
            flash()->error('Настройки сохранить не удалось');

            return view('admin.menu.index', [
                'menu' => $menu,
                'tab' => $this->request->tab
            ]);
        }
    }

    private function tabs() {
        return [
            'items'   => 'Товары',
            'content' => 'Контент',
        ];
    }

    private function tab_contents() {

        $items = Item::live()
            ->orderBy('created_at', 'desc')
            ->get(['id', 'name as title', 'slug', 'category_id'])
            ->groupBy('category_id');


        $item_categories = $items->map(function ($values, $category_id) {
            return ItemCategory::find($category_id);
        });

        $contents = Content::live()
            ->orderBy('created_at', 'desc')
            ->get(['id', 'title', 'slug', 'category_id'])
            ->groupBy('category_id');


        $content_categories = $contents->map(function ($values, $category_id) {
            return ContentCategory::find($category_id);
        });


        $tab_contents = [
            'items'   => [
                'resources'   => $items,
                'categories'  => $item_categories,
                'routes' => [
                    'index_route' => ['items.index' => ['title', 'category_id']],
                    'show_route'  => ['items.show'  => ['title', 'seo_id']],
                ]
            ],

            'content' => [
                'resources'   => $contents,
                'categories'  => $content_categories,
                'routes' => [
                    'index_route' => ['contents.index' => ['title', 'category_id']],
                    'show_route'  => ['contents.show'  => ['title', 'category_id', 'seo_id']],
                ]
            ],


        ];

        return $tab_contents;
    }

    private function menu_params() {

        return [
            'left_menu'  => array_get($this->request->menu, 'left_menu', []),
            'right_menu' => array_get($this->request->menu, 'right_menu', [])
        ];
    }

}
