<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Item;
use App\Models\Item\Category;

class ItemsController extends Controller
{
    public function __construct(Request $request)
    {

        if ($request->route('items')) {
            $this->item = Item::live()->bySlug($request->route('items'))->first();
        }

        if ($request->route('category_id')) {
            $this->category = Category::find( $request->route( 'category_id' ) );

            if ( is_null( $this->category ) ) {
                abort(404);
            }
        }


    }

    public function index(Request $request)
    {
        $product_line_options = \App\DynamicAttribute::options_for('product_line');

        $filter = in_array( $request->filter, array_keys($product_line_options) ) ? $request->filter : null;
        $items  = Item::live()
            ->byCategoryId($this->category->id)
            ->orderBy('price', 'asc')
            ->get();

        if ( $this->category->id == 'mattress' )
        {

            if ($filter) {
                $items = $items->filter(function ($item) use ($filter) {
//                return starts_with($item->product_line, $filter);
                    return $item->product_line == $filter;
                });
            }


            $items = $items->sortBy(function ($item, $key) {
                return $item->price_per_square_meter( 'discount' );
            });
        }




        return view('items.index', [
            'items'    => $items,
            'category' => $this->category,
            'filter'   => $filter,
            'navbar'   => $this->navbar($product_line_options),
        ]);
    }

    public function show($id)
    {
        return view('items.show', [
            'item' => $this->item,

        ]);
    }



    private function navbar($options)
    {

        $navbar = [
            null => ['text' => 'Все', 'title' => ''],
        ];

        foreach($options as $key => $title)
        {
            $navbar[$key] = [
                'text'  => "Серия {$title}",
                'title' => $title
            ];
        }

        return $navbar;
    }

}
