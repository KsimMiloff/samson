<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Item;
use App\Models\Settings\Calculator;
use App\Order;

use Mail;
use Cart;


class CartController extends Controller
{

    public function index(Request $request)
    {

        $cart = Cart::getContent()->sort();

//        var_dump();
//        exit;

        return view('cart.index', [
            'cart' => $cart,
            'total' => Cart::getTotal(),
            'cart_msg' => $request->cart_msg
        ]);
    }

    public function store(Request $request, $id)
    {

        $attrs = [];

        $item = $this->prepare_item($id, $request);

        if ( $this->valid_size($item) )
        {
            $props = $item->props ?: [];

            $cart_id = $item->cart_id;

            if ($item->has_discount())
            {
                $d = $item->discount;
                $attrs['has_discount'] = true;
                $attrs['price_without_discount'] = $item->price( 'real' );
                $attrs['discount_title'] = $d['title'] . ' - ' . $d['percent'] . '%';
            }

            $attrs = array_merge($attrs, $props);
            $attrs = array_merge($attrs, ['prop_keys' => array_keys($props)]);
            $attrs = array_merge($attrs, ['xfields' => $item->xfields]);
            $attrs = array_merge($attrs, $item->toArray());

            Cart::add( $cart_id, $item->name, $item->cart_price, 1, $attrs );
        }


        if ($request->ajax()) {
            return view('partials._cart_status', [
                'cart_is_empty' => Cart::isEmpty(),
                'cart_total' => Cart::getTotalQuantity()
            ]);
        } else {
            return redirect()->route(
                "cart.index"
            );
        }
    }


    public function update(Request $request, $id)
    {
        Cart::update($id, array(
            'quantity' => array(
                'relative' => false,
                'value' => $request->quantity
            )
        ));

        return redirect()->route(
            "cart.index"
        );
    }


    public function destroy(Request $request, $id)
    {
        Cart::remove($id);

        return redirect()->route(
            "cart.index"
        );
    }


    public function buy(Request $request)
    {
        $cart = Cart::getContent()->sort();

        $order = new Order([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'total_price' => Cart::getTotal(),
        ]);

        $order->cart = $cart->map(function ($cart_item, $cart_id) {
            return [
                'name' => $cart_item->name,
                'quantity' => $cart_item->quantity,
                'price' => $cart_item->price,
                'group_price' => $cart_item->getPriceSum(),
                'size' => $cart_item->attributes->size,
                'category_id' => $cart_item->attributes->category_id,
                'props' => collect($cart_item->attributes)->only($cart_item->attributes->prop_keys),
                'xfields' =>
                    collect(
                        $cart_item
                            ->attributes
                            ->xfields
                    )
                        ->filter(
                            function ($xfield) {
                                if ($xfield->allowed_values) {

                                    return true;
                                }
                            })
                        ->map(
                            function ($xfield) {
                                return ['allowed_values' => $xfield->allowed_values];
                            }
                        )
            ];
        });

        if ($order->save()) {
            $this->notify_about_purchase( $cart, $order );
            Cart::clear();
        }

        return redirect()->route(
            "cart.index", [
                'order' => $order,
                'cart_msg' => 'Ваш заказ №' . $order->id . ' принят'
            ]
        );
    }


    private function valid_size($item)
    {
        if ($item->category_id == 'mattress')
        {
            list( $w, $h ) = explode( 'x', $item->size );
            if ( $w == '0' || $h == '0' ) {
                return false;
            }
        }

        return true;
    }


    private function calculate_price($vars, $width, $height)
    {
        $calc_vars = $vars
            ->map(function ($values) {
//            return collect($values)->flatten()->toArray();
                return collect($values['to_cart'])->values()->toArray();
            });

        $calc_vars['d'] = is_array($calc_vars['d']) ? array_get($calc_vars['d'], 0, 0) : 0; // если массив берем первый элемент или присваем 0

        $calculator = Calculator::read();

        return $calculator->calculate(
            $calc_vars['b'],
            $calc_vars['l1'],
            $calc_vars['l2'],
            $calc_vars['m'],
            $calc_vars['d'],
            $calc_vars['g'],
            $width,
            $height
        );
    }

    private function prepare_item($id, $request)
    {
//        Cart::clear();
        if ($id == 'custom_item') {
            $item = $this->make_custom_item( $request );
        } else
        {
            $item = Item::live()->find($id);

            if ($item->category_id == 'mattress' || $item->category_id == 'cover' )
            {
                $item = $this->setup_size( $item, $request );

            } else
            {
                $item->cart_price = $item->price( 'cart' );
            }

        }

        $item->cart_id = $id . $item->size . $item->cart_price;

        if (! empty( $item->props ) )
        {
            foreach ($item->props as $key => $prop) {
                $item->cart_id .= json_encode($prop); // получаем уникальный ключ из значений, json_encode преобразует в строку, все, даже массив
            }
        }

        return $item;
    }



    private function make_custom_item($request)
    {
        $vars = collect($request->only(['b', 'l1', 'l2', 'm', 'd', 'g']));
        $item_field_values = $vars->pluck('to_cart')
            ->filter(function ($value) {
                return is_array($value);
            })
            ->collapse();

        $item = new Item ([
            'name' => 'Индивидуальный заказ',
            'category_id' => 'mattress',
        ]);

        foreach ($item_field_values as $alias => $value) {
            $item->$alias = $value;
        }

        $item->cart_price = $this->calculate_price($vars, $request->width, $request->height);
        $item->size = $request->width . 'x' . $request->height;

        return $item;
    }



    private function setup_size( $item, $request )
    {
        if ( empty ( $request->to_cart['size'] ) )
        {
            $item->size = $request->to_cart['width'] . 'x' . $request->to_cart['height'];
        }
        else
        {
            $item->size = $request->to_cart['size'];
        }

        $item->cart_price = $item->sized_price( [ 'type' => 'cart', 'size' => $item->size ] );

        return $item;

    }


    private function notify_about_purchase($cart, $order)
    {

        Mail::send('cart.email.for_customer', ['cart' => $cart, 'order' => $order], function ($m) use ($order) {
            $m->from('info@sam-son.kz', 'Матрасы «СамСон»');
            $m->to($order->email, 'Самсон')->subject('Новый заказ!');
        });

        Mail::send('cart.email.for_vendor', ['cart' => $cart, 'order' => $order], function ($m)  {

            $m->from('info@sam-son.kz', 'Матрасы «СамСон»');
            $m->to('almaty_matress@mail.ru', 'Самсон')->subject('Новый заказ!');
        });

    }

}