<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Content;
use App\Models\Content\Category;

class ContentsController extends Controller
{

    public function __construct(Request $request)
    {

        if ($request->route('id')) {
            $this->content = Content::bySlug($request->route('id'))->first();
        }

        if ($request->route('category_id')) {
            $this->category = Category::find( $request->route( 'category_id' ) );

            if ( is_null( $this->category ) ) {
                abort(404);
            }
        }
    }


    public function index(Request $request)
    {
        $contents  = Content::live()->visible()
            ->byCategoryId($this->category->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('contents.index', [
            'contents' => $contents,
            'category' => $this->category,
        ]);
    }

    public function show(Request $request)
    {
        if ( $this->is_need_redirection( $request->id ) )
        {
            return $this->redirect_to_canonical();
        }

        return view('contents.show', [
            'content' => $this->content,

        ]);
    }


    public function is_need_redirection($id)
    {
        return ($this->content->id == $id);
    }



    public function redirect_to_canonical()
    {
        return redirect()->route(
            "contents.show", [
                'category_id' => $this->content->category_id,
                'id' => $this->content->seo_id
            ]
        );

    }




}
