<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Content;


class WelcomeController extends Controller
{

    public function index()
    {
        $advices = Content::live()->visible()->forMainPage()
            ->orderBy('created_at', 'desc')
            ->byCategoryId('advice')
            ->limit(4)->get();

        $intro   = Content::live()->visible()->forMainPage()
            ->orderBy('created_at', 'desc')
            ->byCategoryId('page')
            ->first();

        return view('welcome.index', [
            'intro' => $intro,
            'advices' => $advices
        ]);
    }

}
