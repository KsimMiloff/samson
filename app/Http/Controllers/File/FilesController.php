<?php

namespace App\Http\Controllers\File;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\File\FileEntry;

use Illuminate\Http\Response;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $entries = Fileentry::all();

        return view('fileentries.index', compact('entries'));
    }

    public function store(Request $request) {
        $file  = $request->file('file', ['multiple' => false]);
        $entry = FileEntry::upload($file);

        if($request->ajax()){
            return response()->json(['file_id' => $entry->id]);
        }

        return "HTTP";
    }

    public function wisiwyg_upload(Request $request) {
        $file  = $request->file('upload', ['multiple' => false]);
        $entry = FileEntry::upload($file);

        $funcNum = $_GET['CKEditorFuncNum'];
        $url     = route('images.show', ['id' => $entry->id]);
        $message = '';


        return "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";

    }

    public function show($id) {

        $entry = Fileentry::find($id);

        return (new Response($entry->file(), 200))
            ->header('Content-Type', $entry->mime);
    }
}
