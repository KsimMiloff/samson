<?php

namespace App\Http\Controllers\File;

use App\Models\File\ImageEntry;

use Illuminate\Http\Response;

class ImagesController extends FilesController
{

    public function resize($id, $size, $crop=false)
    {
        $crop = $crop == 'crop' ? true : false;
        list ($width, $height) = str_contains($size, 'x') ? explode('x', $size) : [$size, $size];

        $width  = $width  ?: null;
        $height = $height ?: null;

        $entry = ImageEntry::find($id);
        $resized_file  = $entry->resize($width, $height, $crop);

        return (new Response($resized_file, 200))
            ->header('Content-Type', $entry->mime);
    }
}
