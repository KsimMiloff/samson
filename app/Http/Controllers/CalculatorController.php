<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Settings\Calculator;
use App\Item;
use App\Models\Item\Category;

class CalculatorController extends Controller
{

    public function index(Request $request)
    {
        if ($request->has('item_id')) {
            $item = Item::live()->find($request->item_id);
        } else {
            $item = null;
        }

        $width  = $request->width;
        $height = $request->height;

        $calculator = Calculator::read();

        return view('calculator.index', [
            'calculator' => $calculator,
            'item'       => $item,
            'width'      => $width,
            'height'     => $height,
        ]);
    }
}
