<?php

namespace App\Http\Middleware;

use Closure;
use Cart;

class CartMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        view()->share('cart_is_empty', Cart::isEmpty());
        view()->share('cart_total', Cart::getTotalQuantity());

        return $next($request);
    }
}
