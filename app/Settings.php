<?php

namespace App;

use App\Models\Item\Category as ItemCategory;
use App\Models\Concerns\DynamicMethods;
use App\Models\Concerns\DynamicMethods\Mutators;

use App\Models\Concerns\SeoableMixin;



class Settings extends BaseModel
{
    use DynamicMethods;
    use Mutators;
    use SeoableMixin;


    protected $table = 'settings';


    protected $guarded = [];

    protected $casts = [
        'props' => 'array',
    ];

    protected $rules = array(
//        'setname' => 'required|unique:settings',
    );

    function associate_props($alias, $setter_default=null) {
        $getter = $this->getter_mutator($alias);
        $setter = $this->setter_mutator($alias);

        $this->addMethod($getter, function () use ($alias, $setter_default) {
            return array_get( $this->props, $alias, $setter_default );
        });

        $this->addMethod($setter, function ($value) use ($alias) {
            $props = $this->props; // как всегда eloquent не дает нам присвоить напрямик, используем переменную-посредника
            $this->props = array_set($props, $alias, $value);
        });
    }

}
