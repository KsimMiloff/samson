<?php

namespace App;


class DynamicAttribute extends BaseModel
{

    protected $table = 'dynamic_attributes';

    protected $guarded = [];

    protected $attributes = [];

    protected $rules = array(
        'name' => 'required',
        'key' => 'required',
        'title' => 'required',
    );


    protected $casts = [    ];


    public static $passible_attrs = [
        'product_line' => 'Линия продукции',
        'spring_block' => 'Пружинный блок',
        'cover' => 'Покрытие',
    ];

    private static $attr_name = null;
    private static $new_values = [];
    private static $existed_attrs = [];
    private static $atts_to_update = [];


    public function scopeLive($query) {
        return $query->where('is_deleted', false);
    }


    public static function save_attribute_values($attr_name, $values)
    {
        static::$attr_name = $attr_name;
        static::assign_values( $values );

        try {
            foreach( static::$atts_to_update as $attr )
            {
                $attr->save();
            }
        }
        catch( Exception $e ) { return false; }

        return true;
    }


    public static function options_for( $name )
    {
        $options = [];
        $attr_values = DynamicAttribute::where([ 'name' => $name ])
            ->live()
            ->orderBy( 'position' )
            ->get();
        foreach($attr_values as $attr)
        {
            $options[$attr['key']] = $attr['title'];
        }

        return $options;
    }


    public static function assign_values($values)
    {
        static::setup_keys2values( $values );
        static::get_existed_attrs();
        static::update_existed_attrs();
        static::make_new_attrs();
        static::set_attr_position();
    }


    private static function setup_keys2values( $values )
    {
        // генерим пары латинский_ключ => кирилическое_значение
        static::$new_values = collect( $values )
            ->map(function ($title, $key) {
                $title = trim( $title );
                $key   = static::generate_key( $title );
                return [$key => $title];
            })
            ->collapse()
            ->toArray();
    }


    private static function get_existed_attrs()
    {
        $keys = array_keys(static::$new_values);
        // получаем список так или иначе затронутых атрибутов
        static::$existed_attrs = DynamicAttribute::where(['name' => static::$attr_name])
            ->get()
            ->filter(
                function ($attr) use ($keys) {
                    return (
                        $attr->is_deleted == false || // если это текущие (не удаленные) атрибуты
                        array_key_exists($attr->key, $keys) // или атрибуты пришедшие из формы
                    );
                }
            );

    }


    private static function update_existed_attrs()
    {
        foreach (static::$existed_attrs as $attr)
        {
            // если этот атрибут был в списке переданном из формы
            if (isset(static::$new_values[$attr->key]))
            {
                // то обновляем найденные атрибуты
                $attr->fill([
                    'title' => static::$new_values[$attr->key],
                    'is_deleted' => false, // на случай, если атрибут был удален в прошлом
                ]);
            } else {
                // иначе этот атрибут требует удаления
                $attr->is_deleted = true;
            }

            static::$atts_to_update[] = $attr;
        }
    }


    private static function make_new_attrs()
    {
        // получаем id-шники уже созданных атрибутов
        $existed_keys = static::$existed_attrs
            ->pluck('key')->toArray();

        $not_existed_values = [];
        foreach (static::$new_values as $key => $title) {
            if (!in_array($key, $existed_keys)) {
                $not_existed_values[$key] = $title;
            }
        }


        foreach( $not_existed_values as $key => $title )
        {
            if (! in_array( $key, $existed_keys ))
            {
                static::$atts_to_update[] = DynamicAttribute::create([
                    'name' => static::$attr_name,
                    'is_deleted' => false,
                    'title' => $title,
                    'key' => $key
                ]);
            }
        }
    }


    private static function set_attr_position()
    {
        foreach( array_values( static::$new_values ) as $position => $title )
        {
            foreach( static::$atts_to_update as $attr )
            {
                if ( $attr->title == $title )
                {
                    $attr->position = $position;
                }
            }
        }
    }


    private static function generate_key( $title )
    {
//        // полагаемся на предположение о том, что целочисленный ключ - это еще не заданный ключ
//        // поэтому если ключ целочисленный, то генерим новый на основании заголовка
//        // иначе оставляем старый
//        $k = is_int($key) ? str_slug($title) : $key;
        $special_keys = static::keys_for_special_titles( $title );

        return $special_keys ? $special_keys : str_slug($title);
    }


    private static function keys_for_special_titles( $title )
    {
        // говно-функция, но она нужна для совместимости старой версии сайта, когда значения хранились в конфиге
        // и текущей, когда они вводятся пользователем и хранятся в БД, особенно важно для сохранения с калькулятором

        // тут мы указываем список старых (конфиговых) значений для заголовков, которые УЖЕ хранятся в базе
        // если пользователь в форме указал один из нижеследующих заголовков, то мы не генерим для него key, а берем
        // из списка.
        $special_titles_keys = [
            'product_line' => [
                'bs'   => 'BS',
                'ps'   => 'PS',
                'ws'   => 'WS',
                'mp'   => 'MP',
                'ep'   => 'Evo Premium',
                'b'   =>  'Дети',
            ],

            'spring_block' => [
                'bs'   => 'Зависимый',
                'ps'   => 'Независимый',
                'ws'   => 'Безпружинный',
                'mp512'  => 'MP 512',
                'mp1000' => 'MP 1000',
                'mp2x'   => 'MP 2X',
            ],

            'cover' => [
                'thermofelt' => 'Термовойлок',
                'coir' => 'Кокосовая койра',
                'latex' => 'Латекс',
                'memorics' => 'Меморикс',
                'hollkon' => 'Холлкон',
                'spanbond' => 'Спанбонд',
                'ppu_wabe' => 'ППУ ячеистый',
                'ppu_1sm' => 'ППУ лицевой 1см',
                'ppu_2sm' => 'ППУ лицевой 2см',
            ]
        ];



        $special_key = collect( $special_titles_keys[static::$attr_name] )
            ->flip()
            ->first(
                function( $t, $k ) use ( $title ) {
                    return str_slug( $t ) == str_slug( $title );
                }
            );

        return $special_key;

    }
}