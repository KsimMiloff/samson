<?php

    use App\Models\Item\Category;

    function mattress_sizes_select($name, $with_blank = false)
    {
        $options = prices_per_square_meter()
            ->map( function($value, $key) use ($with_blank) {

                $title = $value;

                if (is_null($value))
                {
                    if (! $with_blank)
                    {
                        return '';
                    }

                    $title = 'за кв. м.';
                }

                $selected = $key == 'default' ? 'selected' : '';

                return "<option {$selected} value='{$value}'>{$title}</option>";
            })
            ->implode('');

        return sprintf(
            "<select name='{$name}' data-behavior='change-square'>%s</select>", $options
        );
    }

    function price_placeholder($default_price, $price_per_square_meter, $class='')
    {

        $prices = [];
        foreach (prices_per_square_meter() as $key => $value)
        {
            if (is_null($value))
            {
                $price = $price_per_square_meter;
            } else {
                list($w, $h) = explode('x', $value);
                $price = ($w * $h) * 0.0001 * $price_per_square_meter;
            }

            $selected = $key == 'default' ? 'selected' : '';
            $price = prettify_price($price);

            $prices[$value] = $price;
        }

        return sprintf(
            "<span class='%s' data-values='%s'>%s</span>", $class, json_encode( $prices ), price_with_currency($default_price)
        );
    }

    function price_with_currency($price, $class='price-value') {
        $price = prettify_price($price);
        return "<span class='{$class}'>{$price}</span><span> ₸</span>";
    }

    function prettify_price($price) {
        return number_format($price, 0, ',', ' ');
    }


//    function sized_price($price_per_square_meter, $size) {
//        list($w, $h) = explode('x', $size);
//        return ($w * $h) * 0.0001 * $price_per_square_meter;
//    }


    function prices_per_square_meter()
    {
        return collect(Category::$mattress_sizes)
            ->prepend(null);
    }
