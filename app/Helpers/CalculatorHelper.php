<?php
    function var_values($calculator, $item, $alias, $options=[])
    {
        $variable = find_variable($calculator, $alias);

        $item_value = null;
        if (isset ( $item ) && isset( $item->$alias )) {
            $item_value = $item->$alias;
        }

        $html = '';

        if ( count( $variable ) ) {
            $var_name = array_get($options, 'var_name', array_keys($variable)[0]);
            $sets     = array_values($variable)[0];

            $alias  = $sets['alias'];
            $values = $sets['alias_values'];
            $only   = array_get($options, 'only', null);
            $required = (int) $sets['required'];

//            $item_value = array_get($options, 'item_value', null);

            if ( $only )
            {
//                print_r($values);

                $values = collect( $values )->only( $only )->toArray();
//                print_r($values);
//                exit;
            }


            if ($sets['multi']) {
                $input_type = 'checkbox';
            } else {
                $input_type = 'radio';
            }

            $input_type = array_get($options, 'input_type', $input_type);

            foreach ($values as $value => $title) {
                if ( $calculator->$value )
                {
                    if (is_array( $item_value ))
                    {
                        $checked = array_key_exists( $value, $item_value ) && $item_value[$value];
                    } else {
                        $checked = $item_value == $value;
                    }

                    $input_name = $var_name . "[to_cart][$alias]";

                    if ($sets['multi']) {
                        $input_name .= '[]';
                    }

                    $input_html  = Form::$input_type($input_name, $value, $checked, ['data-calc-weight' => $calculator->$value, 'data-calc-required' => $required]);
                    $input_html .= '<span>' . $title . '</span>';

                    $html .= '<label class="checker">' . $input_html . '</label>';
                }
            }

        }
        return $html;
    }

    function var_title($calculator, $alias, $options=[])
    {
        $variable = find_variable($calculator, $alias);
        $html = '';

        if ( count( $variable ) )
        {
            $sets  = array_values($variable)[0];
            $html .= "<span class='label'>" . $sets['title'] . "</span>";
            $html .= "<br>";
        }

        return $html;
    }


    function find_variable($calculator, $alias)
    {
        return
            collect( $calculator->variables_list() )
                ->filter( function($variable) use ($alias)
                {
                    return $variable['alias'] == $alias;
                })->toArray();
    }