<?php

Form::macro('wisiwyg', function($name, $value=null, $options=[]) {

    $id    = array_get($options, 'id') ?: $name. '_wisiwyg';
    array_set($options, 'id', $id);

    $field = Form::textarea($name, $value, $options);

    $upload_dir = route('files.wisiwyg_upload', ['_token' => csrf_token() ]);
    return "
        <div class='wisiwyg_container'>
            $field
        </div>
        <script>
            $(function() {
                CKEDITOR.replace( '{$id}', {
                    filebrowserUploadUrl: '{$upload_dir}'
                });
            });
        </script>
    ";

});


Form::macro('boolean', function($name, $checked=null, $options=[]) {

    if ($checked) {
        $options['checked'] = true;
    }

    $value = array_pull($options, 'value', 1);

    $field = Form::hidden($name, 0) . Form::checkbox($name, $value, $options);

    return $field;

});