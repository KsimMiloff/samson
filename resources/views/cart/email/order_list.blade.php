<table class="cart-table">
    <colgroup>
        <col width="30%"/>
        <col width="35%"/>
        <col width="15%"/>
        <col width="15%"/>
        <col />
    </colgroup>
    <tr>
        <th>Товар</th>
        <th>Характеристики</th>
        <th>Количество</th>
        <th>Цена</th>
    </tr>

    @foreach($cart as $cart_i)

        <?php
        $item = $cart_i->attributes;
        ?>

        <tr>
            <td>
                @if ( $item->image_id )
                    {!! Html::picture($item->image_id, ['size' => '90x50', 'crop' => true]) !!}
                @endif

                <b>{!! $cart_i->name !!}</b>
            </td>

            <td>
                <small>
                    @if(View::exists("items.category.{$item->category_id}._props"))
                        @include("items.category.{$item->category_id}._props", ['item' => $item])
                    @endif
                </small>
            </td>

            <td>

                {!! $cart_i->quantity !!} шт.

            </td>

            <td>{!! prettify_price( $cart_i->getPriceSum() ) !!} <span> ₸</span></td>

        </tr>
    @endforeach

</table>