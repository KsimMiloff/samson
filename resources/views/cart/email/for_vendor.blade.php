<p>Заказ №{!! $order->id !!}</p>

<p>Время и дата заказа: {!! date( "d.m.Y H:i:s" ) !!}</p>

@include( 'cart.email.order_list' )

<p>Адрес доставки:</p>
{!! $order->address !!}