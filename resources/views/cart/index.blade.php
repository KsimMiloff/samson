@extends('layouts.site')

@section('title')
    <div class="wavy-line wavy-bottom">
        <div class="container">
            <div class="pv-25">
                @include('partials.breadcrumbs', ['crumbs' => [
                    ['url' => '/', 'title' => 'Главная'],
                    ['active' => true, 'title' => 'Корзина'],
                ]])

                <h1 class="h1">
                    Корзина
                </h1>

            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="container">
        <div class="pv-25">

            @if ($cart->isEmpty())
                <div class="content">
                    @if( empty( $cart_msg ) )
                        <?php $cart_msg = 'Ваша корзина пуста'; ?>
                    @endif

                    {!! $cart_msg !!}

                </div>
            @else

                <div class="cart-container">
                    <div class="cart">
                        <table class="cart-table">
                            <colgroup>
                                <col width="30%"/>
                                <col width="35%"/>
                                <col width="15%"/>
                                <col width="15%"/>
                                <col />
                            </colgroup>
                            <tr>
                                <th>Товар</th>
                                <th>Характеристики</th>
                                <th>Количество</th>
                                <th>Цена</th>
                                <th></th>
                            </tr>

                            @foreach($cart as $cart_i)

                                <?php
                                    $item = $cart_i->attributes;
                                ?>

                                <tr>
                                    <td>
                                        @if ( $item->image_id )
                                            {!! Html::picture($item->image_id, ['size' => '90x50', 'crop' => true]) !!}
                                        @endif

                                        <b>{!! $cart_i->name !!}</b>
                                    </td>

                                    <td>
                                        <small>
                                            @if(View::exists("items.category.{$item->category_id}._props"))
                                                @include("items.category.{$item->category_id}._props", ['item' => $item])
                                            @endif
                                        </small>
                                    </td>

                                    <td>

                                        {!! Form::open(['route' => ['cart.update', 'cart_id' => $cart_i->id]]) !!}
                                            {!! Form::hidden('_method', "PUT") !!}

                                            {!! Form::number('quantity', $cart_i->quantity, ['step' => 1, 'min' => 1, 'class' => 'xsmall']) !!}

                                            {!! Form::button('', ['type' => 'submit', 'class' => 'icon-btn flaticon-recalculate']) !!}
                                        {!! Form::close() !!}

                                    </td>

                                    <td>{!! prettify_price( $cart_i->getPriceSum() ) !!} <span> ₸</span></td>

                                    <td>

                                        {!! Form::open(['route' => ['cart.destroy', 'cart_id' => $cart_i->id]]) !!}
                                            {!! Form::hidden('_method', "DELETE") !!}
                                            {!! Form::button('', ['type' => 'submit', 'class' => 'icon-btn flaticon-delete']) !!}
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach

                        </table>

                        <div class="cart-total-container">
                            <table>
                                <tr>
                                    <td>
                                        <b>итого:</b> <b class="cart-total">{!! prettify_price( $total ) !!} </b><b> ₸</b>
                                    </td>
                                    <td>
                                        <button type="button" class="btn" data-target="#contacts-data" data-behavior="buy">Оформить</button>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div id="contacts-data" style="display: none;">

                            <div class="wavy-line wavy-bottom">
                                <div class="form-body">
                                    <h3 class="h1">
                                        Как вас найти?
                                    </h3>
                                </div>
                            </div>

                            {!! Form::open(['route' => ['cart.buy']]) !!}

                                <div class="form-body">

                                    <div>
                                        {!! Form::label('name', 'Имя*') !!}
                                        {!! Form::text('name', null, ['required']) !!}
                                    </div>

                                    <div>
                                        {!! Form::label('phone', 'Телефон*') !!}
                                        {!! Form::text('phone', null, ['required', 'data-behavior' => 'phone']) !!}
                                    </div>

                                    <div>
                                        {!! Form::label('email', 'E-mail*') !!}
                                        {!! Form::email('email', null, ['required']) !!}
                                    </div>

                                    <div>
                                        {!! Form::label('address', 'Адрес доставки*') !!}
                                        {!! Form::textarea('address', null, ['required']) !!}
                                    </div>


                                    <div>
                                        {!! Form::button('Купить', ['type' => 'submit', 'class' => 'btn']) !!}
                                    </div>

                                </div>

                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>

            @endif

        </div>
    </div>

    <script>

        $phones = $( '[data-behavior=phone]' );
        $phones.mask( "+7 (999) 999-99-99" );


//        $phones.on( "blur", function() {
//            var last = $( this ).val().substr( $(this).val().indexOf("-") + 1 );
//
//            if( last.length == 3 ) {
//                var move = $(this).val().substr( $(this).val().indexOf("-") - 1, 1 );
//                var lastfour = move + last;
//
//                var first = $(this).val().substr( 0, 9 );
//
//                $(this).val( first + '-' + lastfour );
//            }
//        });

    </script>

@stop

