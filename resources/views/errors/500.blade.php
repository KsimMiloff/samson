<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <link href="/favicon.bmp" rel="shortcut icon" type="image/x-icon" />

    <title>500 ошибка</title>

    <link href="{{ asset('/vendor/styles/reset.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/styles/site/http_errors.css') }}" rel="stylesheet">

</head>
<body>
    <div class="wrapper">
        <div class="body">
            <a href="/" class="logo">
                <img src="/assets/images/error-logo.png">
            </a>

            <h1>500 Ошибка</h1>


            <p>На сайте, что-то сломалось. :(</p>
            <p>Попробуйте вернуться на <a href="/">главную</a> страницу или ознакомиться с нашим <a href="#">каталогом</a>.</p>
        </div>
    </div>
</body>
</html>