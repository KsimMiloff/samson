<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <link href="/favicon.bmp" rel="shortcut icon" type="image/x-icon" />

    <title>Страница не найдена</title>

    <link href="{{ asset('/vendor/styles/reset.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/styles/site/http_errors.css') }}" rel="stylesheet">

</head>
    <body>
        <div class="wrapper">
            <div class="body">
                <a href="/" class="logo">
                    <img src="/assets/images/error-logo.png">
                </a>

                <h1>404, Страница не найдена.</h1>


                <p>Неправильно набран адрес, или такой страницы на сайте больше не существует.</p>
                <p>Вернитесь на <a href="/">главную</a> страницу или ознакомьтесь с нашим <a href="#">каталогом</a>.</p>
            </div>
        </div>
    </body>
</html>