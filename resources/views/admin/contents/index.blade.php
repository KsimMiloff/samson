@extends('layouts.admin')

@section('content')

    <div class="row">

        <h1>Контент

            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Добавить... <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    @foreach ( $categories as $category )
                        <li> {!! link_to_route('admin.contents.create', $category->title, ['category_id' => $category->id], []) !!} </li>
                    @endforeach
                </ul>
            </div>

            <div class="pull-right">
                @include('admin.partials.lifecycle_menu', ['route' => 'admin.contents.index'])
            </div>

        </h1>

        @if ($contents->isEmpty())
            <p>Нет ни одного контента</p>

        @else

            <table class="table table-hover">
                <colgroup>
                    <col width="70%"/>
                </colgroup>
                <thead>
                    <tr>
                        <th>Заголовок</th>
                        <th>Тип</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($contents as $content)
                        <tr>

                            <td> {!! link_to_route('admin.contents.edit', $content->title, ['content_id' => $content->id]) !!} </td>
                            <td> {!! $content->category->title !!} </td>

                        </tr>

                    @endforeach

                </tbody>
            </table>

        @endif

    </div>
@stop