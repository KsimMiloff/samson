<div class="row">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#info">Инфо</a></li>
        <li class=""><a data-toggle="tab" href="#seo">SEO</a></li>
    </ul>


    {!! Form::resource($content, ['route' => 'admin.contents', 'class' => 'form-horizontal']) !!}

        {!! Form::hidden('content[category_id]', $content->category_id) !!}

        <div class="tab-content">

            @include('admin.partials.errors', ['errors' => $content->errors()])

            @include("admin.contents.form._info_tab")
            @include("admin.partials.form._seoable_tab", ['seoable' => $content, 'seoble_type' => 'content'])

        </div>

        @include("admin.partials.form._button_bar", ['object' => $content, 'lifecycled' => true])


    {!! Form::close() !!}

</div>
