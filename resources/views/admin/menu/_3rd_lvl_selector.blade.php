<div class="tab-content">

    <?php
        $resources  = $content['resources'];
        $routes     = $content['routes'];
        $categories = $content['categories'];
    ?>

    @foreach($resources as $category_id => $objects)

        <div role="tabpanel" class="tab-pane" id="{!! $category_id !!}" >

            <div class="top-buffer">


                <select name="menu" class="form-control" data-behavior="">
                    <option data-link-attrs='{!! collect( ['values' => ['route' => collect($routes['index_route'])->keys()[0], 'category_id' => $category_id, 'title' => $categories[$category_id]->plural_title]] )->toJson() !!}'>
                        Общий список
                    </option>

                    @foreach($objects as $key => $object)

                        <?php $data = [] ?>

                        @foreach($routes['show_route'] as $route => $params)

                            <?php $data["values"]['route'] = $route ?>

                            @foreach($params as $param)

                                <?php $data["values"][$param] = $object->$param ?>

                            @endforeach

                        @endforeach

                        <option data-link-attrs='{!! collect($data)->toJson() !!}'>
                            {!! $object->title !!}
                        </option>
                    @endforeach

                </select>

                <div class="modal-footer top-buffer">
                    <div class="row">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" data-behavior="add_menu_item">Добавить в меню</button>
                    </div>
                </div>

            </div>
        </div>
    @endforeach
</div>

