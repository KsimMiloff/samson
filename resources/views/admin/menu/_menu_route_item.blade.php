<span href="#" class="list-group-item handle">

    <div class="pull-right">
        <a href="#" class="glyphicon glyphicon-remove" data-behavior="remove-menu-item"></a>
    </div>

    <span class="menu-item-title">{{ $menu_item['title'] }}</span>

    <a href="#" class="glyphicon glyphicon-edit" data-behavior="edit-menu-item"></a>

    @foreach($route_settings as $set_group)
        @foreach($set_group as $route_values)

            @if (array_has($route_values, $menu_item['route']))

                @foreach ($route_values[$menu_item['route']] as $fieldname)

                    @unless ($fieldname=='title')
                        @if(array_key_exists($fieldname, $menu_item))
                            <input type="hidden" name="menu[{{ $menu_type }}][{{ $timestamp }}][{{ $fieldname }}]" value="{{ $menu_item[$fieldname] }}"/>
                        @endif
                    @endunless

                @endforeach

            @endif

        @endforeach
    @endforeach


    <div class="menu-item-form" style="display: none; ">
        <div class="col-sm-10">
            <input type="text" name="menu[{{ $menu_type }}][{{ $timestamp }}][title]" value="{{ $menu_item['title'] }}" class="form-control"/>
        </div>
        <a class="btn btn btn-default" data-behavior="save-menu-item" href="#"><i class="glyphicon glyphicon-floppy-save"></i></a>
    </div>

    <input type="hidden" name="menu[{{ $menu_type }}][{{ $timestamp }}][route]" value="{{ $menu_item['route'] }}"/>


</span>