@extends('layouts.admin')

@section('scripts')
    <script src="{{ asset('/vendor/scripts/Sortable.js') }}" type="text/javascript" charset="utf-8" ></script>
@stop


@section('content')

    <div class="page-header">
        <h1>Меню</h1>
    </div>

    {!! Form::resource($menu, ['route' => 'admin.menu', 'class' => 'form-horizontal']) !!}

        <div class="form-group">

            @foreach (['left_menu' => 'до логотипа', 'right_menu' => 'после логотипа'] as $menu_type => $menu_title)

                {{--{!! Form::hidden("menu[{$menu_type}][]") !!}--}}

                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {!! $menu_title !!}
                            <div class="pull-right">
                                <a href="#" class="btn btn-xs btn-default" data-behavior="open_menu_item_dialog" data-menu-target="#{!! $menu_type !!}" data-toggle="modal" data-target="#new_menu_Item_modal">Добавить</a>
                            </div>
                        </div>

                        <div class="list-group dnd" id="{!! $menu_type !!}">
                            @foreach($menu->$menu_type as $timestamp => $menu_item)
                                <?php
                                    $route_settings = collect($tab_contents)->pluck('routes');
                                ?>

                                @include("admin.menu._menu_route_item", [
                                    'menu_item' => $menu_item,
                                    'timestamp' => $timestamp,
                                    'menu_type' => $menu_type,
                                    'route_settings' => $route_settings
                                    ])
                            @endforeach
                        </div>

                    </div>
                </div>
            @endforeach

        </div>

        <div class="form-group">
            <div class="col-sm-8">
                {!! Form::button('Сохранить', ['type' => 'submit', 'class' => 'btn btn-primary']) !!}

                <div class="pull-right">
                    {!! link_to(URL::previous(), 'Отмена', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>

        </div>

    {!! Form::close() !!}

    @include("admin.menu._modal", ['tabs' => $tabs, 'tab_contents' => $tab_contents])


    <script>
        $(function() {


            var lmenu = document.getElementById("left_menu");
            var rmenu = document.getElementById("right_menu");

            $.each([ lmenu, rmenu ], function( index, menu ) {
                Sortable.create(menu, {
                    handle: ".handle",
                    draggable: "span",
                    filter: 'input[type=text]',
                    group: 'menu',
                    animation: 200,

                    onAdd: function (evt) {
                        from = $(evt.from).attr('id');
                        to   = $(evt.to).attr('id');

                        $menu_item = $(evt.item);
                        new_html   = $menu_item.html().split(from).join(to);
                        $menu_item.html(new_html);
                    },
                });
            });

            add_menu_route = function($list, values) {

                time = Math.floor(Date.now() / 1000);
                side = $list.attr('id');

                inputs = '';

                $.each(values, function( key, value ) {
                    if (key == 'title') {
                        inputs += '' +
                            '<div class="menu-item-form" style="display: none;">' +
                                '<div class="col-sm-10">' +
                                    '<input type="text" name="menu[' + side + '][' + time + '][' + key + ']" value="' + value + '"  class="form-control"/>' +
                                '</div>' +
                                '<a class="btn btn btn-default" data-behavior="save-menu-item" href="#"><i class="glyphicon glyphicon-floppy-save"></i></a>' +
                            '</div>'
                    } else {
                        inputs += '<input type="hidden" name="menu[' + side + '][' + time + '][' + key + ']" value="' + value + '"/>';
                    }
                });
                a = '' +
                    '<span href="#" class="list-group-item handle">' +

                        '<div class="pull-right">' +
                            '<a href="#" class="glyphicon glyphicon-remove" data-behavior="remove-menu-item"></a>' +
                        '</div>' +

                        '<span class="menu-item-title">' + values['title'] + '</span>' +
                        ' <a href="#" class="glyphicon glyphicon-edit" data-behavior="edit-menu-item"></a>' +
                        inputs +
                    '</span>';


                $list.append( $( a ) );
            };


            $('#new_menu_Item_modal').on('shown.bs.modal', function() {
                $('.nav li, .tab-pane').removeClass('active');
                $(this).find('form')[0].reset();
            });

            $(document).on('click', '[data-behavior=open_menu_item_dialog]', function() {
                modal_id    = $(this).data('target');
                menu_target = $(this).data('menu-target');
                $modal      = $(modal_id);
                $modal.data('menu-target', menu_target);
            });


            $(document).on('click', '[data-behavior=add_menu_item]', function() {
                target_id = $(this).closest('.modal').data('menu-target');
                $list = $(target_id);
                $active_pane = $(this).closest('.tab-pane .active');

                link_attrs = $active_pane.find('[name=menu]').find(":checked").data('link-attrs');
                values = link_attrs['values'];
                add_menu_route($list, values);

            });

            $(document).on('click', '[data-behavior=add-calculator-link]', function() {
                target_id = $(this).closest('.modal').data('menu-target');
                $list = $(target_id);
                values = {route: 'calculator', title: 'Калькулятор'}

                add_menu_route($list, values);

            });


            $(document).on('click', '[data-behavior=edit-menu-item]', function() {
                $(this).closest('.handle')
                        .find('.menu-item-form')
                        .show();

                $('[data-behavior=remove-menu-item]').hide();
            });


            $(document).on('click', '[data-behavior=save-menu-item]', function() {
                $handle = $(this).closest('.handle');
                $menu_item = $handle.find('.menu-item-form');
                $menu_item_title = $handle.find('.menu-item-title');

                $menu_item_value = $menu_item.find('input').val();

                $menu_item.hide();
                $menu_item_title.html($menu_item_value);

                $('[data-behavior=remove-menu-item]').show();
            });


            $(document).on('click', '[data-behavior=remove-menu-item]', function() {
                $(this).closest('.handle').remove();
            });

        });

    </script>
@stop