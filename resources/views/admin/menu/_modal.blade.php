<div class="modal" id="new_menu_Item_modal" role="dialog">
    <form>
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Добавить элемент меню</h4>
                </div>

                <div class="modal-body">


                    <ul class="nav nav-pills nav-pills-default" role="tablist">
                        @foreach($tabs as $tab => $title)
                            <li role="presentation" class=''>
                                <a href="#{!! $tab !!}" aria-controls="" role="tab" data-toggle="tab">{!! $title !!}</a>
                            </li>
                        @endforeach

                        <div class="pull-right">
                            <a href="#" class="btn btn-primary" data-behavior="add-calculator-link" data-dismiss="modal">Калькулятор</a>
                        </div>
                    </ul>

                    <div class="tab-content top-buffer">
                        @foreach($tab_contents as $tab => $content)


                            <div role="tabpanel" class="tab-pane" id="{!! $tab !!}">
                                @include("admin.menu._2nd_lvl_selector", ['resources' => $content['resources'], 'categories' => $content['categories']])
                                @include("admin.menu._3rd_lvl_selector", ['content' => $content])
                            </div>

                        @endforeach
                    </div>
                </div>

                {{--<div class="modal-footer">--}}
                    {{--<div class="pull-right">--}}
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                    {{--</div>--}}
                {{--</div>--}}

            </div>
        </div>
    </form>
</div>