<ul class="nav nav-pills nav-pills-default" role="tablist">

    @foreach($resources as $category_id => $objects)

        <li role="presentation" class=''>
            <a href="#{!! $category_id !!}" aria-controls="" role="tab" data-toggle="tab">{!! $categories[$category_id]->plural_title !!}</a>
        </li>

    @endforeach

</ul>