@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <h1>Настройка переменных калькулятора</h1>
    </div>

    <div class="alert alert-info">
        <p>Используемая калькулятором формула: <strong><em>( ( B + L1 + L2 + M + G ) * S + D * P) * K</em></strong>, где:</p>
        <p>S - площать матраса.</p>
        <p>B - пружинный блок;</p>
        <p>L - слои;</p>
        <p>M - материал обивки;</p>
        <p>D - боковое ППУ;</p>
        <p>G - усиление пружинного блока;</p>
        <p>K - коэффициент маржинальности;</p>
        <p>** Площадь (S) задается пользователем!</p>
    </div>

    <div class="row">

        <div class="tab-content">
            <div id="info" class="tab-pane fade in active">

                @include('admin.partials.errors', ['errors' => $calculator->errors()])


                <div class="row top-buffer">


                    {!! Form::resource($calculator, ['route' => 'admin.calculator', 'class' => 'form-horizontal']) !!}

                    <?php
                        $active_tab = isset($tab) ? $tab : array_keys( $calculator->groups() )[0];
                        $is_active_tab = [];
                    ?>


                    <ul class="nav nav-tabs">
                        @foreach ($calculator->groups() as $group => $values)
                            <?php $group == $active_tab ? array_set($is_active_tab, $group, 'active') : array_set($is_active_tab, $group, '') ?>

                            <li class="{{ $is_active_tab[$group] }}"><a data-toggle="tab" href="#{{ $group }}">&laquo;{!! $group !!}&raquo;</a></li>
                        @endforeach

                    </ul>

                    <div class="tab-content top-buffer">
                        @foreach ($calculator->groups() as $group => $values)

                            <div id="{{ $group  }}" class="tab-pane fade in {{ $is_active_tab[$group] }}">
                                <div class="row top-buffer">

                                    @foreach ($values as $alias => $title)
                                        <?php
                                            //var_dump($alias);
                                            //exit;
                                            ?>
                                        <div class="form-group">
                                            {!! Form::label($alias, $title, ['class' => 'control-label col-sm-2'])  !!}

                                            <div class="col-sm-2">
                                                {!! Form::number("calculator[{$alias}]", $calculator->$alias, array('class' => 'form-control', 'min' => 0)) !!}
                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="form-group">
                                        <div class="row top-buffer">
                                            <div class="col-sm-8 col-md-offset-2">
                                                {!! Form::button('Сохранить', ['type' => 'submit', 'name' => 'tab', 'value' => $group, 'class' => 'btn btn-primary']) !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        @endforeach
                    </div>


                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop