<div class="btn-group">

    <?php
    $state_menu = [
            'live' => 'Живые',
            'archive' => 'Архивные'
    ];

    $active_state = $state ? $state : 'live';
    ?>

    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {!! $state_menu[$active_state] !!} <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        @foreach($state_menu as $state => $state_title)
            <li>{!! link_to_route($route, $state_title, ['state' => $state ], []) !!}</li>
        @endforeach
    </ul>
</div>