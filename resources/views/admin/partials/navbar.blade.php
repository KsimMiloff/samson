<?php

use App\Order;

$new_orders_count = Order::where('state', 'not_viewed')->count();

$active = [];

$menu = [
        /*'ItemsController' => [
                'title' => 'Tовары',
                'url' => route('admin.items.index'),
        ],*/

        'dropdown'  => [
                'title' => 'Каталог',
                'subs' => [
                        'ItemsController' => [
                                'title' => 'Tовары',
                                'url'   => route('admin.items.index'),
                        ],
                        'DynamicAttributesController' => [
                                'title' => 'Найстройки',
                                'url'   => route('admin.dynamic_attributes.index'),
                        ],
                ]
        ],

        'ContentsController'  => [
                'title' => 'Контент',
                'url' => route('admin.contents.index'),
        ],
        'OrdersController'  => [
                'title' => 'Заказы',
                'url' => route('admin.orders.index'),
                'badge' => $new_orders_count,
        ],
        'CalculatorController'  => [
                'title' => 'Калькулятор',
                'url' => route('admin.calculator.index'),
        ],
        'DiscountController'  => [
                'title' => 'Скидки',
                'url' => route('admin.discounts.index'),
        ],
        'MenuController'  => [
                'title' => 'Меню',
                'url' => route('admin.menu.index'),
        ],

    /*'dropdown'  => [
        'title' => 'Настройки',
        'subs' => [
            'CalculatorController' => [
                'title' => 'Калькулятор',
                'url'   => route('admin.calculator.index'),
            ],
            'MenuController' => [
                'title' => 'Меню',
                'url'   => route('admin.menu.index'),
            ],
        ]
    ],*/
];

foreach ($menu as $item => $options)
{
    if ( $item == 'dropdown' )
    {
        $active[$item] = $item;

        foreach ($options['subs'] as $subitem => $options)
        {
            if ( $subitem == $controller )
            {
                $active[$subitem] = 'active';
                $active[$item] = 'active';
            } else {
                $active[$subitem] = '';
            }
        }

    } else {
        $active[$item] = $item == $controller ? 'active' : '';
    }
}

$active[$controller] = isset($active[$controller]) ? $active[$controller] : '';

//var_dump($active);

?>

<ul class="nav navbar-nav">


    @if (Auth::check())

        @foreach ($menu as $item => $options)

            <li class="{{ $active[$item] }}">

                @if ( $item == 'dropdown' )

                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        {{ $options['title'] }}  <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        @foreach ($options['subs'] as $subitem => $options)

                            <li class="{{ $active[$subitem] }}">
                                <a href="{{ $options['url'] }}">
                                    {{ $options['title'] }}
                                </a>
                            </li>

                        @endforeach
                    </ul>

                @else
                    <a href={{ $options['url'] }}>
                        {{ $options['title'] }}

                        @if (isset($options['badge']) && $options['badge'])
                            <span class="badge">{!! $options['badge'] !!}</span>
                        @endif
                    </a>
                @endif
            </li>

        @endforeach
    @endif
</ul>

<ul class="nav navbar-nav navbar-right">
    @if (Auth::guest())
        <li>{!! link_to_route('auth.login', 'Войти') !!}</li>
    @else
        @can('watch-users')
            <li>{!! link_to_route('admin.users.index', 'Пользователи') !!}</li>
        @endcan

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
               aria-expanded="false">{{ Auth::user()->name }}
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li>{!! link_to_route('auth.logout', 'Выйти') !!}</li>
            </ul>
        </li>
    @endif
</ul>