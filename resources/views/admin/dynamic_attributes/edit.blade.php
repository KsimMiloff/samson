@extends('layouts.admin')

@section('content')

    <style>
        .attrs-container .input-group:last-of-type .toggle-icon:before {
            content: '\002b';
        }
    </style>

    <div class="page-header">
        <h1>Редактирование - "{!! \App\DynamicAttribute::$passible_attrs[ $attr_name ] !!}"</h1>
    </div>

    <div class="row top-buffer">

        {!! Form::open( [ 'route' => 'admin.dynamic_attributes.save', 'class' => 'form-horizontal'] ) !!}

            {!! Form::hidden('attribute[name]', $attr_name) !!}


            <div class="form-group">
                <div class="col-sm-4">
                    <div class="attrs-container">


                        @foreach( $attributes as $attribute )
                            <div class="input-group new-attr">
                                {!! Form::text("attribute[titles][{$attribute->key}]", $attribute->title, array('class' => 'form-control')) !!}

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-behavior="append">
                                        <span class="glyphicon glyphicon-minus toggle-icon"></span>
                                    </button>
                                </span>
                            </div>

                        @endforeach


                        <div class="input-group new-attr">
                            {!! Form::text('attribute[titles][]', '', array('class' => 'form-control')) !!}

                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" data-behavior="append">
                                    <span class="glyphicon glyphicon-minus toggle-icon"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-4">
                {!! Form::submit('Сохранить', ['name' => 'action[save]', 'class' => 'btn btn-primary']) !!}
            </div>

        {!! Form::close() !!}

    </div>

    <script>
        $ (document ).on('click', '[data-behavior="append"]:not(:last)', function() {
            $self = $( this ).closest( '.new-attr' ).remove();
        });

        $ (document ).on('click', '[data-behavior="append"]:last', function() {
            $parent   = $( this ).closest( '.attrs-container' );
            $new_attr = $( this ).closest( '.new-attr' ).clone();
            $new_attr.find( 'input').val('');
            $parent.append( $new_attr );
        });
    </script>
@stop
