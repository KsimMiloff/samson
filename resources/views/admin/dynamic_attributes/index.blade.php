@extends('layouts.admin')


@section('content')

    <div class="row">

        <h1>Настройки магазина - динамические атрибуты</h1>

        <table class="table table-hover">
            {{--<colgroup>--}}
                {{--<col width="70px"/>--}}
            {{--</colgroup>--}}
            <thead>
                <tr>
                    <th>Название</th>
                </tr>
                </thead>
            <tbody>

                @foreach ($attributes as $attr => $title)
                    <tr>
                        <td> {!! link_to_route('admin.dynamic_attributes.edit', $title, ['item_id' => $attr]) !!} </td>
                    </tr>

                @endforeach

            </tbody>
        </table>


    </div>
@stop