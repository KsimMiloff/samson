<div class="row">

    {!! Form::resource($discount, ['route' => 'admin.discounts', 'class' => 'form-horizontal']) !!}


        @include('admin.partials.errors', ['errors' => $discount->errors()])

        <div class="row top-buffer">

            <div class="form-group">
                {!! Form::label('discount[title]', 'Заголовок', ['class' => 'control-label col-sm-2'])  !!}
                <div class="col-sm-8">
                    {!! Form::text('discount[title]', $discount->title, array('class' => 'form-control')) !!}
                </div>
            </div>


            <div class="form-group">
                {!! Form::label('discount[percent]', '%', ['class' => 'control-label col-sm-2'])  !!}
                <div class="col-sm-2">
                    {!! Form::text('discount[percent]', $discount->percent, array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-8 col-sm-offset-2">
                    <ul class="nav nav-tabs">
                        @foreach ($groups as $type => $title)
                            <li class="{{ $type==array_keys($groups)[0] ? 'active' : '' }}"><a data-toggle="tab" href="#{{$type}}">{{$title}}</a></li>
                        @endforeach
                    </ul>

                    <div class="tab-content">
                        @foreach ($groups as $type => $title)
                            <div id="{{$type}}" class="tab-pane fade {{ $type==array_keys($groups)[0] ? 'active in' : '' }}">
                                <div class="row top-buffer">

                                    @foreach($items as $item)
                                        @if( starts_with( $item->spring_block, $type ))
                                            <label>
                                                {!! Form::boolean("discount[item_ids][]", $discount->items->contains( $item ), ['value' => $item->id]) !!}
                                                {!! $item->name !!}
                                            </label>
                                            <br/>
                                        @endif
                                    @endforeach

                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>


            {{--<div class="form-group">--}}
                {{--<div class="col-sm-3 col-sm-offset-2">--}}
                    {{--<div class="checkbox">--}}
                        {{--<label>--}}
                            {{--{!! Form::boolean('discount[is_stoped]', $discount->is_visible) !!} Показывать на сайте--}}
                        {{--</label>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>



    <div class="form-group">
        <div class="row top-buffer">
            <div class="col-sm-8 col-md-offset-2">
                {!! Form::submit('Сохранить', ['name' => 'action[save]', 'class' => 'btn btn-primary']) !!}

            </div>
        </div>
    </div>


    {!! Form::close() !!}

</div>
