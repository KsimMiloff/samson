@extends('layouts.admin')

@section('content')
    <div class="row">

        <h1>Акции

            {!! link_to_route('admin.discounts.create', 'Добавить', [], ['class' => 'btn btn-primary dropdown-toggle']) !!}

        </h1>

        @if ($discounts->isEmpty())
            <p>Нет ни одного контента</p>

        @else

            <table class="table table-hover">
                <colgroup>
                    <col width="70%"/>
                </colgroup>
                <thead>
                <tr>
                    <th>Заголовок</th>
                    <th>%</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($discounts as $discount)
                    <tr>

                        <td> {!! link_to_route('admin.discounts.edit', $discount->title, ['content_id' => $discount->id]) !!} </td>
                        <td> {!! $discount->percent !!} </td>

                    </tr>

                @endforeach

                </tbody>
            </table>

        @endif

    </div>
@stop