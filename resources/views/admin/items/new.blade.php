@extends('layouts.admin')

@section('content')
    @include('admin.partials.breadcrumbs', ['crumbs' => $crumbs])

    <div class="page-header">
        <h1>{!! $crumbs->last()['title'] !!} - {!! $item->category->plural_title !!}</h1>
    </div>

    @include('admin.items.form')
@stop
