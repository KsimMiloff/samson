<div id="info" class="tab-pane fade in active">

    <div class="row top-buffer">

        <div class="form-group">
            {!! Form::label('name', 'Название', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text('item[name]', $item->name, array('class' => 'form-control')) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('extra_text', 'Пояснение', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::text('item[extra_text]', $item->extra_text, array('class' => 'form-control', 'rows' => 5)) !!}
            </div>
        </div>

        @if($item->exists())
            @include("admin.items.form._discount_section")
        @endif

        @foreach($item->xfields() as $alias => $field)
            {!! $field->widget(['model' => 'item', 'alias' => $alias, 'value' => $item->$alias]) !!}
        @endforeach


        <div class="form-group">
            {!! Form::label('desc', 'Описание', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::wisiwyg('item[desc]', $item->desc, array('class' => 'form-control')) !!}
            </div>
        </div>


        <div class="form-group">
            {!! Form::label('image_id', 'Изображение', ['class' => 'control-label col-sm-2'])  !!}
            <div class="col-sm-8">
                {!! Form::images('item[image_id]', $item->image_id) !!}
            </div>
        </div>

        {{--<div class="form-group">--}}
        {{--{!! Form::label('slider_ids', 'Слайдер', ['class' => 'control-label col-sm-2'])  !!}--}}
        {{--<div class="col-sm-8">--}}
        {{--{!! Form::images('item[slider_ids]', $item->slider_ids, ['multiple' => true]) !!}--}}
        {{--</div>--}}
        {{--</div>--}}

        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-2">
                <div class="checkbox">
                    <label>
                        {!! Form::boolean('item[is_visible]', $item->is_visible) !!} Показывать на сайте
                    </label>
                </div>
            </div>
        </div>


    </div>
</div>