@if ($item->category_id == 'mattress' && $item->exists)

    <div class="form-group">
        {!! Form::label('discount', 'Скидка', ['class' => 'control-label col-sm-2'])  !!}
        <div class="col-sm-8">


            <div class="col-sm-8">
                <ul class="nav nav-pills">
                    @foreach (['current' => 'Текущая', 'new' => 'Создать скидку'] as $tabid => $tabname)
                        <li class="{{ $tabid=='current' ? 'active' : '' }}"><a data-toggle="tab" href="#{{$tabid}}">{{$tabname}}</a></li>
                    @endforeach
                </ul>

                <div class="tab-content col-sm-offset-0">


                    <div id="current" class="tab-pane fade active in">
                        <div class="row top-buffer">
                            {!! Form::select('item[discount_id]', [null => 'Без скидки'] + $discounts, $item->discount_id, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div id="new" class="tab-pane fade">
                        <div class="row top-buffer">

                            <div class="form-group">
                                {!! Form::label('discount[title]', 'Заголовок', ['class' => 'control-label col-sm-3'])  !!}
                                <div class="col-sm-9">
                                    {!! Form::text('discount[title]', '', array('class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('discount[percent]', '%', ['class' => 'control-label col-sm-3'])  !!}
                                <div class="col-sm-2">
                                    {!! Form::text('discount[percent]', '', array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--{!! Form::label('discount[price]', 'или Цена', ['class' => 'control-label col-sm-3'])  !!}--}}
                                {{--<div class="col-sm-9">--}}
                                    {{--{!! Form::text('discount[price]', '', array('class' => 'form-control')) !!}--}}
                                {{--</div>--}}
                            {{--</div>--}}

                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
@endif