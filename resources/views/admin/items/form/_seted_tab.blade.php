<div id="seted" class="tab-pane fade in">

    <div class="row top-buffer">

        <?php
            list($active) = $category_siblings->keys();
        ?>

        @if ($category_siblings->count())
            <ul class="nav nav-pills">
                @foreach ($category_siblings as $cid => $category)
                    <li class="{!! $cid == $active ? 'active' : '' !!}">
                        <a data-toggle="tab" href="#sibling-{!! $cid !!}">{!! $category->plural_title !!}</a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content">

                @foreach ($category_siblings as $cid => $category)
                    <div id="sibling-{!! $cid !!}" class="top-buffer tab-pane fade in {!! $cid == $active ? 'active' : '' !!}">

                        @foreach($items_for_set[$cid] as $good)

                            <div style="float:left; width: 25%">

                                <label>
                                    <div style="display: table-cell; min-width: 100px;">

                                        <div class="thumbnail">
                                            @if ($good->image_id)
                                                {!! Html::picture($good->image_id, ['size' => '100', 'crop' => true]) !!}
                                            @else
                                                <div class="thumb-empty"><span class="glyphicon glyphicon-minus"></span></div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="checkbox" style="display: table-cell; vertical-align: top;">
                                        {!! Form::boolean("item[seted_ids][]", $item->in_my_set($good->id), ['value' => $good->id]) !!} {!! $good->name !!}
                                    </div>

                                </label>
                            </div>

                        @endforeach
                    </div>
                @endforeach

            </div>
        @endif

    </div>
</div>