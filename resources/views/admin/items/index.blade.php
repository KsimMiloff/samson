@extends('layouts.admin')

@section('content_bar')

    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <span class="navbar-brand">Фильтровать по...</span>
                </div>

                <div class="collapse navbar-collapse navbar-form navbar-left pull-right" id="bs-example-navbar-collapse-2">
                    {!! Form::open(['route' => 'admin.items.index', 'method' => 'get']) !!}


                    <div class="form-group">
                        {!! Form::select(
                                'category_id',
                                collect($categories)->map( function($category, $key) {
                                    return $category->plural_title;
                                }),
                                $category_id,
                                ['placeholder' => 'Категории...', 'class' => "form-control"]
                        ) !!}
                    </div>

                    <button type="submit" class="btn btn-primary glyphicon glyphicon-filter"></button>

                    {!! Form::close() !!}
                </div>

            </div>
        </nav>
    </div>

@stop

@section('content')

    <div class="row">

        {{--<div class="col-md-2">--}}
        {{--</div>--}}

        <h1>Товары

            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Добавить... <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    @foreach ( $categories as $category )
                        <li> {!! link_to_route('admin.items.create', $category->title, ['category_id' => $category->id], []) !!} </li>
                    @endforeach
                </ul>
            </div>

            <div class="pull-right">
                @include('admin.partials.lifecycle_menu', ['route' => 'admin.items.index'])
            </div>
        </h1>

        @if ($items->isEmpty())
            <p>Нет ни одного товара</p>

        @else

            <table class="table table-hover">
                <colgroup>
                    <col width="70px"/>
                </colgroup>
                <thead>
                    <tr>
                        <th></th>
                        <th>Название</th>
                        <th>Категория</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($items as $item)
                        <tr>
                            {!! Form::resource($item, ['route' => 'admin.items', 'route_params' => ['redirect_to' => 'index'], 'class' => 'form-horizontal']) !!}

                            <td>
                                <div class="thumbnail">
                                    @if ($item->image_id)
                                        {!! Html::thumbed_link_to_picture($item->image_id, ['target' => 'blank'], ['size' => '50', 'crop' => true]) !!}
                                    @else
                                        <div class="thumb-empty"><span class="glyphicon glyphicon-minus"></span></div>
                                    @endif
                                </div>
                            </td>
                            <td> {!! link_to_route('admin.items.edit', $item->name, ['item_id' => $item->id]) !!} </td>
                            <td> {!! $item->category->title !!} </td>

                            <td>
                                <div class="pull-right">
                                    @if ($item->is_live)
                                        {!! Form::submit('В архив', ['name' => 'action[move_to_archive]', 'class' => 'btn btn-primary']) !!}
                                    @elseif ($item->is_archive)
                                        {!! Form::submit('Восстановить', ['name' => 'action[move_to_live]', 'class' => 'btn btn-primary']) !!}
                                    @endif
                                </div>

                            </td>

                            {!! Form::close() !!}
                        </tr>

                    @endforeach

                </tbody>
            </table>

        @endif

    </div>
@stop