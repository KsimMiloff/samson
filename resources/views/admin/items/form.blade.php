<div class="row">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#info">Инфо</a></li>
        <li class=""><a data-toggle="tab" href="#seo">SEO</a></li>
        <li class=""><a data-toggle="tab" href="#seted">Сопутствующие товары</a></li>
    </ul>


    {!! Form::resource($item, ['route' => 'admin.items', 'class' => 'form-horizontal']) !!}

        {!! Form::hidden('item[category_id]', $item->category_id) !!}

        <div class="tab-content">

            @include('admin.partials.errors', ['errors' => $item->errors])

            @include("admin.items.form._info_tab")
            @include("admin.partials.form._seoable_tab", ['seoable' => $item, 'seoble_type' => 'item'])
            @include("admin.items.form._seted_tab")

        </div>

        @include("admin.partials.form._button_bar", ['object' => $item, 'lifecycled' => true])


    {!! Form::close() !!}

</div>
