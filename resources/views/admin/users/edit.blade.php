@extends('layouts.admin')

@section('content')
    @include('admin.partials.breadcrumbs', ['crumbs' => $crumbs])

    <div class="page-header">
        <h1>{!! $crumbs->last()['title'] !!} </h1>
    </div>

    @include('admin.users.form')
@stop
