<div class="row">

    <div class="tab-user">
        <div id="info" class="tab-pane fade in active">

            @if($user->email == Auth::user()->email)
                <div class="alert alert-warning" role="alert">Внимание, это ваша учетная запись, не выстрелите себе в ногу!</div>
            @endif

            @include('admin.partials.errors', ['errors' => $user->errors])


            <div class="row top-buffer">


                {!! Form::resource($user, ['route' => 'admin.users', 'class' => 'form-horizontal']) !!}

                <div class="form-group">
                    {!! Form::label('user[name]', 'Имя', ['class' => 'control-label col-sm-2'])  !!}
                    <div class="col-sm-4">
                        {!! Form::text('user[name]', $user->name, array('class' => 'form-control')) !!}
                    </div>
                </div>

                @unless ($user->exists)
                    <div class="form-group">
                        {!! Form::label('email', 'E-mail', ['class' => 'control-label col-sm-2'])  !!}
                        <div class="col-sm-3">
                            {!! Form::email('email', $user->email, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                @endunless


                <div class="form-group">
                    {!! Form::label('password', 'Новый пароль', ['class' => 'control-label col-sm-2'])  !!}
                    <div class="col-sm-3">
                        {!! Form::password('password', array('class' => 'form-control')) !!}
                    </div>
                </div>


                @can('toggle-admin-state')
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <div class="checkbox">
                                <label>
                                    {!! Form::boolean('is_admin', $user->is_admin) !!} Админ
                                </label>
                            </div>

                        </div>
                    </div>
                @endcan


                {{--<div class="form-group">--}}
                    {{--{!! Form::label('pass_confirm', 'Подтвердите пароль', ['class' => 'control-label col-sm-2'])  !!}--}}
                    {{--<div class="col-sm-3">--}}
                        {{--{!! Form::password('pass_confirm', array('class' => 'form-control')) !!}--}}
                    {{--</div>--}}
                {{--</div>--}}



                <div class="form-group">
                    <div class="row top-buffer">
                        <div class="col-sm-8 col-md-offset-2">
                            {!! Form::submit('Сохранить', ['name' => 'action[save]', 'class' => 'btn btn-primary']) !!}

                            <div class="pull-right">

                                {!! link_to(URL::previous(), 'Отмена', ['class' => 'btn btn-primary']) !!}

                                @if($user->email != Auth::user()->email)

                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger">Удалить?</button>
                                        <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                            <span class="sr-only">?</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                {!! Form::submit('Да', ['data-behavior' => 'delete_user', 'class' => 'btn btn-link']) !!}
                                            </li>
                                            <li><button type="button" class="btn btn-link">Нет</button></li>
                                        </ul>
                                    </div>

                                @endif

                            </div>

                        </div>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('[data-behavior=delete_user]').click(function(e) {
            e.preventDefault();

            $form = $(this).closest('form');
            $method_input = $form.find('[name=_method]');
            $method_input.val( 'DELETE' );
            $form.submit()
        });
    });
</script>