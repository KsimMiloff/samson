@extends('layouts.admin')

@section('content_bar')

@stop

@section('content')

    <div class="row">

        <h1>Заказы</h1>

        @if ($orders->isEmpty())
            <p>Нет ни одного заказа</p>

        @else

            <table class="table table-hover">
                <colgroup>
                    <col width="35%"/>
                    <col width="15%"/>
                    <col width="30%"/>
                    <col width="20%"/>
                </colgroup>
                <thead>
                    <tr>
                        <th>Заказ</th>
                        <th>Статус</th>
                        <th>Заказчик</th>
                        <th>Дата заказа</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($orders as $order)

                        <?php
                            $class = '';

                            if ($order->state == 'not_viewed') {
                                $class = 'warning';
                            } elseif ($order->state == 'rejected') {
                                $class = 'danger';
                            } elseif ($order->state == 'approved') {
                                $class = 'success';
                            }

                        ?>

                        <tr class="{!! $class !!}">
                            <td>
                                <b>Общая цена заказа: </b>{!! price_with_currency($order['total_price']) !!};
                                <ul>
                                    @foreach ($order->cart_items as $k => $item )
                                        <li>
                                            {!! $item['name'] !!}
                                            <small>
                                                (
                                                    {!! $item['category']->title !!};
                                                    {!! $item['quantity'] !!} шт;
                                                )
                                            </small>;

                                        </li>

                                    @endforeach
                                </ul>
                            </td>

                            <td>
                                {!! link_to_route('admin.orders.edit', $order->state_title, ['id' => $order->id]) !!}
                            </td>

                            <td>

                                <div>
                                    {!! $order->name !!}:
                                    тел. {!! $order->phone !!}; {!! $order->email !!};
                                    <br/>
                                    Адрес: {!! $order->address !!}
                                </div>

                            </td>

                            <td>
                                {!! $order->created_at !!}
                            </td>

                        </tr>

                    @endforeach

                </tbody>
            </table>

        @endif

    </div>
@stop