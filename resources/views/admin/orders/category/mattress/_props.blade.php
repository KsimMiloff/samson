
<?php
    $upholsteries = $item['xfields']['upholstery']["allowed_values"];
    $covers = $item['xfields']['cover_1']["allowed_values"];
    $side_ppu = $item['xfields']['side_ppu']['allowed_values'];

    $props = $item['props'];

    //var_dump($upholsteries);
    //exit;

?>

@if ( isset( $item['size'] ))
    <li>
        Размер: {!! $item['size'] !!} см
    </li>
@endif


@if ( isset( $props['spring_block'] ))
    <li>
        @if( $props['spring_block'] == 'ws' )
            Беспружинный блок;
        @else
            Пружинный блок:
            @if (isset( $props['spring_block_gain'] ))
                усилиный
            @endif

            @if( $props['spring_block'] == 'bs')
                &laquo;Bonnel Spring&raquo;;
            @else
                &laquo;Pocket Spring&raquo;;
            @endif
        @endif
    </li>
@endif

@if ( isset($props['cover_1']) && isset($props['cover_2']))

    <?php $cover_sum = array_merge( array_keys( $props['cover_1'] ), array_keys( $props['cover_2'] ) ); ?>

    <li>Слои:
        @foreach(array_count_values( $cover_sum ) as $cover => $count)
            @if($count == 2)
                {!! $covers[$cover] !!} с двух сторон;
            @else
                {!! $covers[$cover] !!};
            @endif
        @endforeach
    </li>

@endif


@if ( isset( $props['upholstery'] ) )
    <li>
        Обивка: {!! $upholsteries[$props['upholstery']] !!};
    </li>
@endif

@if( isset( $props['side_ppu'] ) )
    <li>Боковое ППУ {!! $side_ppu[$props['side_ppu']] !!}</li>
@endif

@if( isset( $props['side_upholstery'] ) )
    @if($props['side_upholstery'] == 'air_system_3d')
        <li>
            3D air system;
        </li>
    @endif
@endif
