@extends('layouts.admin')

@section('content')
    @include('admin.partials.breadcrumbs', ['crumbs' => $crumbs])

    <div class="page-header">
        <h1>Просмотр онлайн-заказа</h1>
    </div>

        {{--@include('admin.partials.errors', ['errors' => $order->errors()])--}}

        <div class="row top-buffer">


            {!! Form::resource($order, ['route' => 'admin.orders', 'class' => 'form-horizontal']) !!}


            <div class="form-group">
                {!! Form::label('order[person]', 'Заказчик', ['class' => 'control-label col-sm-2'])  !!}
                <div class="col-sm-8">
                    <div>
                        {!! $order->name !!}:
                        тел. {!! $order->phone !!}; {!! $order->email !!};
                        <br/>
                        Адрес: {!! $order->address !!}
                    </div>
                </div>
            </div>

            @foreach ($order->cart_items as $k => $item )

                <div class="form-group">
                    <b class="control-label col-sm-2"> {!! $item['name'] !!}</b>
                    <div class="col-sm-8">

                        <div class="text-block">

                            {!! $item['category']->title !!} &mdash; цена по группе: <b>{!! price_with_currency($item['group_price']) !!}</b>
                            <ul>
                                @if(View::exists("admin.orders.category.{$item['category_id']}._props"))
                                    @include("admin.orders.category.{$item['category_id']}._props", ['item' => $item])
                                @endif

                            </ul>

                            <div>Количество: {!! $item['quantity'] !!} шт.</div>
                            <div>Цена (шт.): {!! price_with_currency($item['price']) !!}</div>

                        </div>


                    </div>
                </div>


            @endforeach


            <div class="form-group">
                <b class="control-label col-sm-2">Общая цена заказа</b>
                <div class="col-sm-8">
                    <span class="form-text">
                        {!! price_with_currency($order['total_price']) !!}
                    </span>
                </div>
            </div>


            <div class="form-group">
                {!! Form::label('order[state]', 'Статус', ['class' => 'control-label col-sm-2'])  !!}
                <div class="col-sm-2">
                    {!! Form::select('order[state]', $states, $order->state, ['class' => 'form-control']) !!}
                </div>
            </div>


            <div class="form-group top-buffer">
                <div class="col-sm-8 col-md-offset-2">
                    {!! Form::submit('Закрыть', ['name' => 'action[save]', 'class' => 'btn btn-primary']) !!}
                </div>
            </div>

            {!! Form::close() !!}

    </div>

@stop
