<div class="cart-status">
    @if ($cart_is_empty)
        <div class="cart-count">
            Корзина: нет товаров
        </div>
    @else
        <div class="cart-count">
            Корзина: {!! $cart_total !!} товар
        </div>

        {!! link_to_route('cart.index', 'Оформить', [], ['class' => 'cart-show']) !!}
    @endif
</div>