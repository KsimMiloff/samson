<ol class="crumbs">
    @foreach ($crumbs as $crumb)
        @if ( isset($crumb['active']) && $crumb['active'] )
            <li class="active">{!! $crumb['title'] !!}</li>
        @else
            <li>{!! link_to($crumb['url'], $crumb['title']) !!}</li>
        @endif
    @endforeach

</ol>
