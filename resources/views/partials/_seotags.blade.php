@section('seotags')
    <title>{!! $seotags->site_title !!}</title>
    <meta name="keywords" content="{!! $seotags->keywords !!}">
    <meta name="description" content="{!! $seotags->description !!}">
@stop