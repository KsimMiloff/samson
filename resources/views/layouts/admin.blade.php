<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('blog.title') }} Admin</title>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <link href="http://css-spinners.com/css/spinner/three-quarters.css" rel="stylesheet">
        <link href="{{ asset('/assets/styles/admin/admin.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/styles/admin/forms.css') }}" rel="stylesheet">
        @yield('styles')

        <script src="{{ asset('/vendor/scripts/jquery.js') }}"></script>
        <script src="{{ asset('/vendor/scripts/ckeditor/ckeditor.js') }}" type="text/javascript" charset="utf-8" ></script>
        <script src="{{ asset('/vendor/scripts/bootstrap-ckeditor-fix.js') }}"></script>
        <script src="{{ asset('/assets/scripts/admin/forms.js') }}" type="text/javascript" charset="utf-8" ></script>

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        @yield('scripts')
    </head>
    <body>

        {{-- Navigation Bar --}}
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{{ url('/') }}"> Samson</a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        @include('admin.partials.navbar')
                    </div>
                </div>
            </div>
        </nav>

        <div class="container">
            @yield('content_bar')
        </div>

        <div class="container">
            <div class="row">
                @include('flash::message')
            </div>
        </div>

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>