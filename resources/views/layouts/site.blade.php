<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <link href="/favicon.bmp" rel="shortcut icon" type="image/x-icon" />
        {{--<link rel="shortcut icon" href="/favicon.ico"/>--}}


        @yield('seotags')

        <link href="{{ asset('/vendor/styles/reset.css') }}" rel="stylesheet">
        <link href="{{ asset('/vendor/styles/layout.css') }}" rel="stylesheet">

        <link href="{{ asset('/vendor/styles/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
        <link href="{{ asset('/vendor/styles/hover.css') }}" rel="stylesheet">

        <link href="{{ asset('/assets/styles/site/main.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/styles/site/wysiwyg.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/styles/site/icons.css') }}" rel="stylesheet">

        <link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->

        <script src="{{ asset('/vendor/scripts/jquery.js') }}"></script>
        <script src="{{ asset('/vendor/scripts/fancybox/jquery.fancybox.js') }}"></script>
        <script src="{{ asset('/vendor/scripts/jquery.maskedinput.min.js') }}"></script>
        <script src="{{ asset('/assets/scripts/site/site.js') }}"></script>

        <script type="text/javascript" src="http://www.youtube.com/player_api"></script>

        <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->

        @yield('styles')

        {{--<script src="{{ asset('/assets/scripts/admin/forms.js') }}" type="text/javascript" charset="utf-8" ></script>--}}

        @yield('scripts')
    </head>
    <body>

        <?php
            use App\Models\Item\Category;
            use App\Models\Settings\Menu;

            $categories = Category::all();
            $menu = Menu::read();
            $left_menu  = collect($menu->left_menu)->reverse();
            $right_menu = $menu->right_menu;
        ?>


        <a href="/page/1" class="made_in_kz_link">
            <img src="/assets/images/madе-in-kz.png">
        </a>


        <div class="wrapper">
            <header class="blue-top">
                <div class="container">
                    <a href="/">
                        <img class="logo" src="{{ URL::asset('assets/images/logo.png') }}"/>
                    </a>
                    <div class="left top-menu">
                        @foreach ($left_menu as $menu_item)
                            <a class="hvr-underline-from-center" href="{!! route($menu_item['route'], array_except($menu_item, ['route', 'title'])) !!}">{!! $menu_item['title'] !!}</a>
                        @endforeach
                    </div>
                    <div class="right top-menu">
                        @foreach ($right_menu as $menu_item)
                            <a class="hvr-underline-from-center" href="{!! route($menu_item['route'], array_except($menu_item, ['route', 'title'])) !!}">{!! $menu_item['title'] !!}</a>
                        @endforeach
                    </div>
                </div>
            </header>

            <div class="body">
                @yield('breadcrumbs')
                @yield('title')
                @yield('content')
            </div>

        </div>
        <footer class="footer">
            <div class="footer-top">
                <div class="container">
                    <a href="/">
                        <img class="logo" src="{{ URL::asset('assets/images/logo.png') }}"/>
                    </a>

                    <div class="copyright">
                        &copy; 2006&mdash;{{ date('Y') }} &laquo;Алматинская Матрасная Фабрика&raquo;
                    </div>
                    {{--<div class="socbuttons">--}}
                        {{--<ul>Мы в соц. сетях:&nbsp;--}}
                            {{--<li><a href="#"><img src="{{ URL::asset('assets/images/soc-fb.png') }}"/></a></li>--}}
                            {{--<li><a href="#"><img src="{{ URL::asset('assets/images/soc-vk.png') }}"/></a></li>--}}
                            {{--<li><a href="#"><img src="{{ URL::asset('assets/images/soc-inst.png') }}"/></a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                    <div class="contacts">
                        <ul>
                            <a href="/page/7">Контактная информация</a>
                            <li>+7 (727) 393-61-41</li>
                            <li>+7 702 637-21-41</li>
                            <li><a href="mailto:info@samson.kz">info@samson.kz</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89530287-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89530287-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter41861729 = new Ya.Metrika({
                    id:41861729,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/41861729" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    </body>
</html>