<div class='welcome-video'>
    <div class="tabs">

        @foreach($videos as $i => $video)
            @if ($video)
                <div class="tab">

                    <input type="radio" id="tab-{!! $i !!}" name="tab-group-videos" {!! $i == '0' ? 'checked' : '' !!}>
                    <label class='video-tab tab-head' for="tab-{!! $i !!}">
                        {!! $video->title !!}
                    </label>

                    <div class="tab-content">
                        {{--<h3 class="h3">--}}
                        {{----}}
                        {{--</h3>--}}
                        <div class="welcome-video-area">
                            <iframe width="480" height="265" src="http://www.youtube.com/embed/{!! $video->youtube_id !!}?version=3&enablejsapi=1">
                            </iframe>
                        </div>
                    </div>

                </div>
            @endif
        @endforeach
    </div>

    <script>
        $('[name=tab-group-videos]').change(function() {
            $current_tab = $(this).closest('.tab');


            $('.tab')
                .not($current_tab)
                .find('.welcome-video-area iframe')
                .each(
                    function() {

                        // останавливаем видео тремя строками
                        video = $( this ).attr( "src" );
                        $( this ).attr( "src", "" );
                        $( this ).attr( "src", video );
                    }
                );
        });

    </script>

</div>
