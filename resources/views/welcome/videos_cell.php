<?php

use App\Content;
    $videos = Content::live()->visible()->forMainPage()
        ->orderBy('created_at', 'desc')
        ->byCategoryId('video')
        ->get();


    if (! empty( $videos ))
    {

        $video_for_tabs = [];
        foreach ($videos as $video)
        {

            if (! filter_var( $video->youtube_id, FILTER_VALIDATE_URL) === false) {
                $query = parse_url( $video->youtube_id, PHP_URL_QUERY );
                parse_str( $query, $query );
                $video->youtube_id = $query['v'];
            }

            $video_for_tabs[] = $video;
        }

        echo view('welcome.video_tabs', [
            'videos' => $video_for_tabs,
        ]);
    }
?>