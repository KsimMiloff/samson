@extends('layouts.site')

@include('partials._seotags')

@section('content')


    <div class="wavy-line wavy-bottom">
        <div class="container">

            @include("partials._cart_status")


            @if ($intro)
                <div class="welcome-intro">
                    <h1 class="h2">
                        {!! $intro->title !!}
                    </h1>
                    <span class="sub-h1">
                        {!! $intro->intro !!}
                    </span>
                </div>
            @endif
        </div>
    </div>
    <div class="wavy-line wavy-bottom">
        <div class="container">

            <div class="welcome-categories matts-categories">

                <div class="welcome-category mat-1">
                    <a class="wc-link hvr-bounce-to-top hvr-bounce-to-top" href="{!! route('items.index', ['category_id' => 'mattress', 'filter' => 'bs']) !!}">
                        <div class="wc-title">BS</div>
                        <div class="wc-desc">
                            матрасы на зависимых пружинных блоках
                            <br/>
                            Bonnel spring
                        </div>
                    </a>
                </div>
                <div class="welcome-category mat-2">
                    <a class="wc-link hvr-bounce-to-top" href="{!! route('items.index', ['category_id' => 'mattress', 'filter' => 'ps']) !!}">
                        <div class="wc-title">PS</div>
                        <div class="wc-desc">
                            матрасы на независимых пружинных блоках
                            <br/>
                            Pocket spring
                        </div>
                    </a>
                </div>
                <div class="welcome-category mat-3">
                    <a class="wc-link hvr-bounce-to-top" href="{!! route('items.index', ['category_id' => 'mattress', 'filter' => 'ws']) !!}">
                        <div class="wc-title">WS</div>
                        <div class="wc-desc">
                            матрасы с беспружинными блоками
                        </div>
                    </a>
                </div>
            </div>

            <div class="welcome-categories matts-categories">

                <div class="welcome-category mat-4">
                    <a class="wc-link hvr-bounce-to-top hvr-bounce-to-top" href="{!! route('items.index', ['category_id' => 'mattress', 'filter' => 'mp']) !!}">
                        <div class="wc-title">MP</div>
                        <div class="wc-desc">
                            матрасы на пружинных
                            <br/>
                            блоках с повышенной
                            <br/>
                            плотностью пружин на м<sup>2</sup>
                        </div>
                    </a>
                </div>
                <div class="welcome-category mat-5">
                    <a class="wc-link hvr-bounce-to-top" href="{!! route('items.index', ['category_id' => 'mattress', 'filter' => 'ep']) !!}">
                        <div class="wc-title">EVO PREMIUM</div>
                        <div class="wc-desc">
                            матрасы на пружинных блоках c максимальной плотностью пружин на м<sup>2</sup>
                        </div>
                    </a>
                </div>
                <div class="welcome-category mat-6">
                    <a class="wc-link hvr-bounce-to-top" href="{!! route('items.index', ['category_id' => 'mattress', 'filter' => 'b']) !!}">
                        <div class="wc-title">BABY</div>
                        <div class="wc-desc">
                            матрасы разработанные специально для детей и подростков
                        </div>
                    </a>
                </div>
            </div>

            <div class="welcome-categories">
                <div class="welcome-category pillow">
                    <a class="wc-link hvr-bounce-to-top" href="{!! route('items.index', ['category_id' => 'pillow']) !!}">
                        <div class="wc-title">Подушки</div>
                        <div class="wc-desc">
                            по вашим
                            <br/>
                            предпочтениям
                        </div>
                    </a>
                </div>
                <div class="welcome-category cover">
                    <a class="wc-link hvr-bounce-to-top" href="{!! route('items.index', ['category_id' => 'cover']) !!}">
                        <div class="wc-title">Наматрасники</div>
                        <div class="wc-desc">
                            из разных видов
                            <br/>
                            ткани
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="welcome-contents">
            @include("welcome.videos_cell")

            <div class="welcome-articles">
                @unless ($advices->isEmpty())
                    <h3 class="h3">
                        Полезные советы
                    </h3>
                    <ul class="welcome-article-list">
                        @foreach ($advices as $advice)
                            <li>{!! link_to_route( 'contents.show', $advice->title, ['category_id' => $advice->category_id, 'id' => $advice->seo_id] ) !!}</li>
                        @endforeach
                    </ul>
                @endunless
            </div>
        </div>
    </div>
@stop
