<div class="item-el">

    <a href="{!! route('items.show', ['id' => $item->seo_id]) !!}" class="item-el-header">

        <h3 class="h1">

            @yield('el-title', $item->name)

        </h3>

        <div class="item-el-thumb">

            {{--                @yield('el-thumb', Html::picture($item->image_id, ['size' => '285x165', 'crop' => true]))--}}
            @if (empty ($item->image_id) )
                <img src="/assets/images/no-picture-285x165.jpg">
            @else
                {!! Html::picture($item->image_id, ['size' => '285x165', 'crop' => true]) !!}
            @endif

        </div>
    </a>

    <div class="item-el-cart-service">

        <form>

            <div class="item-el-price dynamic-price">

                @include("items.category.{$item->category_id}._item_price")

                {{--@yield('el-price', price_with_currency($item->price))--}}
                {!! Form::hidden('to_cart[price]', $item->price) !!}
                {!! Form::token() !!}

            </div>
            <div class="item-el-btn">
                <button class="btn" type="button" data-cart-url="{!! route('cart.store', ['item_id' => $item->id]) !!}" data-behavior="add-item-to-cart">В корзину</button>
            </div>
        </form>

    </div>

</div>
