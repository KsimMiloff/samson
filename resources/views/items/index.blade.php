@extends('layouts.site')

<?php
    $seotags->title = $category->plural_title;
?>

@include('partials._seotags')

@section('title')
    <div class="wavy-line wavy-bottom">
        <div class="container">
            <div class="pv-25">

                @include("partials._cart_status")

                @include('partials.breadcrumbs', ['crumbs' => [
                    ['url' => '/', 'title' => 'Главная'],
                    ['active' => true, 'title' => $category->plural_title],
                ]])

                @if(View::exists("items.category.{$category->id}._filter"))
                    @include("items.category.{$category->id}._filter", ['active_filter' => $filter])
                @else
                    <h2 class="h1">
                        {!! $category->plural_title !!}
                    </h2>
                @endif

            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="items-container">
        <div class="content pv-12">
            <div class="item-list">

                @forelse ($items as $item)
                    @include("items._item")
                @empty
                    <i>Товары скоро будут ;)</i>
                @endforelse

            </div>
        </div>
    </div>

@stop

