@extends('layouts.site')

@include('partials._seotags', ['seotags' => $item->seotags])

@section('title')
    <div class="wavy-line wavy-bottom">
        <div class="container">
            <div class="pv-25">

                @include("partials._cart_status")

                @include('partials.breadcrumbs', ['crumbs' => [
                    ['url' => '/', 'title' => 'Главная'],
                    ['url' => route('items.index', ['category' => $item->category_id]), 'title' => $item->category->plural_title],
                    ['active' => true, 'title' => $item->name],
                ]])

                <h1 class="h h1">
                    {!! $item->name !!}

                    @if ($item->extra_text)
                        <small>
                            {!! str_limit($item->extra_text, 76) !!}
                        </small>
                    @endif
                </h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="container">
        <div class="content pv-25">
            <div class="item-data-col">



                <?php
                    if($item->has_discount())
                    {
                        $p = $item->discount->percent;
                        switch (true) {
                            case ($p <= 5) : $class = 'lte5'; break;
                            case ($p <= 15) : $class = 'lte15'; break;
                            case ($p <= 25) : $class = 'lte25'; break;
                            case ($p <= 35) : $class = 'lte35'; break;
                            case ($p <= 45) : $class = 'lte45'; break;
                            case ($p <= 55) : $class = 'lte55'; break;
                            case ($p <= 65) : $class = 'lte65'; break;
                            case ($p >= 70) : $class = 'gte70'; break;
                        }
                    }
                ?>



                <div class="item-img-container">
                    @if (empty ($item->image_id) )
                        <img src="/assets/images/no-picture-300x150.jpg">
                    @else
                        {!! Html::thumbed_link_to_picture($item->image_id, ['class' => "fancybox-link flaticon-zoom", 'data-behavior' => 'fancybox-link', 'rel' => 'group'], ['size' => '400x200', 'crop' => true]) !!}
                    @endif

                    @if($item->has_discount())
                        <span class="discount-lbl {!! $class !!}">
                            &ndash; {!! $item->discount->percent !!}%
                        </span>
                    @endif
                </div>

                <div class="desc-tbl">

                    <div class="item-props-container">
                        @if(View::exists("items.category.{$item->category_id}._props"))
                            @include("items.category.{$item->category_id}._props", ['item' => $item])
                        @endif
                    </div>

                    <ul class="item-labels">
                        @if ($item->has_guarantee)
                            <li><span class="item-label label-guarantee">3 года гарантия</span></li>
                        @endif

                        @if ($item->has_o2)
                            <li><span class="item-label label-o2">Дышащие материалы</span></li>
                        @endif

                        @if ($item->has_antibacterial)
                            <li><span class="item-label label-antibacterial">Антибактериальная обработка</span></li>
                        @endif

                        @if ($item->has_antiallergic)
                            <li><span class="item-label label-antiallergic">Антиалергенное покрытие</span></li>
                        @endif
                    </ul>
                </div>

                <div class="item-desc">

                    <h2 class="h2">Описание</h2>
                    <div class="wysiwyg">
                        {!! $item->desc !!}
                    </div>

                </div>
            </div>
            <div class="item-service-col">
                @if(View::exists("items.category.{$item->category_id}._services"))
                    @include("items.category.{$item->category_id}._services", ['item' => $item])
                @endif
            </div>
        </div>
    </div>
@stop