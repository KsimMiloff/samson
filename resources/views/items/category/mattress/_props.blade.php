<ul>

    <?php
        $upholsteries = $item->xfields['upholstery']->allowed_values;
        $covers = $item->xfields['cover_1']->allowed_values;
        $side_ppu = $item->xfields['side_ppu']->allowed_values;


        //var_dump($item->cover_1);
        //var_dump($item->cover_2);
        //exit;
    ?>

    @if ($item->size)
        <li>Размер: {!! $item->size !!} см </li>
    @endif

    @if ($item->spring_block)
        <li>
            @if($item->spring_block == 'ws')
                Беспружинный блок;

            @else
                Пружинный блок:
                @if ($item->spring_block_gain)
                    усилиный
                @endif

                @if($item->spring_block == 'bs')
                    &laquo;Bonnel Spring&raquo;;
                @else
                    &laquo;Pocket Spring&raquo;;
                @endif

            @endif

        </li>
    @endif

    @if ($item->thermofelt)
        <li>Термовойлок с двух сторон;</li>
    @endif


    @if ( is_array( $item->cover_1 ) && is_array( $item->cover_2 ) )
        <li>
            <?php //$cover_sum = array_merge( array_keys($item->cover_1), array_keys($item->cover_2) ); var_dump($cover_sum); exit; ?>
            <?php $coverstypes =  array_keys( $item->cover_1 ); //var_dump($covers); exit; ?>

            @foreach( $coverstypes as $cover_type )
                @if( $item->cover_1[$cover_type]  && $item->cover_2[$cover_type] )
                    <li>
                        {!! $covers[$cover_type] !!} с двух сторон;
                    </li>


                @elseif( $item->cover_1[$cover_type] || $item->cover_2[$cover_type] )
                    <li>
                        {!! $covers[$cover_type] !!};
                    </li>
                @endif
            @endforeach
        </li>
    @endif


    @if ($item->upholstery && array_key_exists($item->upholstery, $upholsteries))
        <li>Обивка: {!! $upholsteries[$item->upholstery] !!};</li>
    @endif

    @if($item->side_ppu)
        <li>Боковое ППУ {!! $side_ppu[$item->side_ppu] !!}</li>
    @endif

    @if($item->side_upholstery)
        <li>3D air system;</li>
    @endif

</ul>