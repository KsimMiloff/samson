@if($item->has_discount())

    <?php
        $p = $item->discount->percent;
        switch (true) {
            case ($p <= 5) : $class = 'lte5'; break;
            case ($p <= 15) : $class = 'lte15'; break;
            case ($p <= 25) : $class = 'lte25'; break;
            case ($p <= 35) : $class = 'lte35'; break;
            case ($p <= 45) : $class = 'lte45'; break;
            case ($p <= 55) : $class = 'lte55'; break;
            case ($p <= 65) : $class = 'lte65'; break;
            case ($p >= 70) : $class = 'gte70'; break;
        }
    ?>

    <span class="discount-lbl {!! $class !!}">
        &ndash; {!! $item->discount->percent !!}%
    </span>

    {!! price_placeholder( $item->price, $item->price_per_square_meter, 'real_price old_price' ) !!}
    {!! price_placeholder( $item->price('discount'), $item->price_per_square_meter( 'discount' ), 'dicounted_price' ) !!}

    <br/>
    {!! mattress_sizes_select('to_cart[size]' ) !!}
@else
    {!! price_placeholder( $item->price, $item->price_per_square_meter, 'real_price' ) !!}
    <br/>

    {!! mattress_sizes_select('to_cart[size]' ) !!}
@endif




