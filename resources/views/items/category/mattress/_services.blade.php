<div class="price-togglers">

    <input type="radio" id="toggler-prepared" class="price-toggler" name="toggler-group-prices" checked>
    <div class="price-toggler-content">
        <div class="item-calc-container">
            <form action="{!! route('calculator') !!}">
                {!! Form::hidden('item_id', $item->id) !!}
                {!! Form::token() !!}

                <div class="item-price-toggler-content dynamic-price">

                    <div class="price-wrapper">

                        <label class='price-toggler' for="toggler-custom">
                            Индивидуальные размеры
                        </label>

                        <div class="prices-list">

                            @if($item->has_discount())
                                {!! price_placeholder( $item->price, $item->price_per_square_meter, 'real_price old_price' ) !!}
                                <br/>
                                {!! price_placeholder( $item->price('discount'), $item->price_per_square_meter( 'discount' ), 'dicounted_price current_price' ) !!}
                                <br/>
                            @else
                                {!! price_placeholder( $item->price, $item->price_per_square_meter, 'current_price real_price' ) !!}
                                <br/>
                            @endif

                            {!! price_with_currency($item->price_per_square_meter( 'discount' ), 'price-per-square_meter') !!}/м<sup><small>2</small></sup>
                        </div>

                    </div>

                    <div>
                        <div class="size-controll">
                            <span class="sizes-title">Стандартные размеры:</span>
                            <br/>
                            {!! mattress_sizes_select('to_cart[size]' ) !!}
                        </div>

                        <button class="btn item-buy-btn" type="button" data-behavior="add-item-to-cart"  data-cart-url="{!! route('cart.store', ['item_id' => $item->id]) !!}">В корзину</button>
                    </div>


                </div>

            </form>
        </div>
    </div>


    <input type="radio" id="toggler-custom" class="price-toggler" name="toggler-group-prices">
    <div class="price-toggler-content">
        <div class="item-calc-container">
            <form action="{!! route('calculator') !!}">
                {!! Form::hidden('item_id', $item->id) !!}
                {!! Form::token() !!}

                <div class="item-price-toggler-content manual-dynamic-price">

                    <div class="price-wrapper">

                        <label class='price-toggler' for="toggler-prepared">
                            Стандартные размеры
                        </label>

                        <div class="prices-list">

                            @if($item->has_discount())
                                <span class="real_price old_price" data-price-per-square-meter="{!! $item->price_per_square_meter !!}">
                                    <?php echo price_with_currency( $item->price ); ?>
                                </span>
                                <br/>

                                <span class="dicounted_price current_price" data-price-per-square-meter="<?php echo $item->price_per_square_meter( 'discount' ); ?>">
                                    <?php echo price_with_currency( $item->price('discount') ); ?>
                                </span>

                                <br/>
                            @else
{{--                                {!! price_placeholder( $item->price, $item->price_per_square_meter, 'current_price real_price' ) !!}--}}
                                <span class="real_price current_price" data-price-per-square-meter="{!! $item->price_per_square_meter !!}">
                                    <?php echo price_with_currency( $item->price ); ?>
                                </span>

                                <br/>
                            @endif

                            {!! price_with_currency($item->price_per_square_meter( 'discount' ), 'price-per-square_meter') !!}/м<sup><small>2</small></sup>
                        </div>

                    </div>

                    <div>

                        <div class="size-controll">
                            <span class="sizes-title">Индивидуальные размеры:</span>
                            <br/>

                            <label style="display: inline-block">
                                {!! Form::number('to_cart[width]', 160, ['min' => 1, 'class' => 'small', 'data-behavior' => 'individulize_size' ]) !!}
                                <br/>
                                ширина
                            </label>

                            <label style="display: inline-block">
                                {!! Form::number('to_cart[height]', 200, ['min' => 1, 'class' => 'small', 'data-behavior' => 'individulize_size' ]) !!}
                                <br/>
                                длина
                            </label>
                        </div>

                        <button class="btn item-buy-btn" type="button" data-behavior="add-item-to-cart"  data-cart-url="{!! route('cart.store', ['item_id' => $item->id]) !!}">В корзину</button>
                    </div>


                </div>

            </form>
        </div>
    </div>


</div>


@if ($item->seted_items->count())

<div class="related-items">
    <div class="related-items-title">
        Сопутствующие товары
    </div>
    @foreach($item->seted_items as $seted)
        <div class="related-item">
            <a href="{!! route('items.show', ['id' => $seted->seo_id]) !!}" class="item-el-header">
                {!! Html::picture($seted->image_id, ['size' => '180x119', 'crop' => true, 'class' => 'related-item-img-container']) !!}
            </a>

            <div class="related-item-title">{!! $seted->name !!}</div>
            <div class="related-item-price">{!! price_with_currency($seted->price) !!}</div>
        </div>
    @endforeach
</div>

@endif