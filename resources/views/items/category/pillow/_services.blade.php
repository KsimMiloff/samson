<div class="price-togglers">

    <div class="item-calc-container">
        <form action="{!! route('calculator') !!}">
            {!! Form::hidden('item_id', $item->id) !!}
            {!! Form::token() !!}

            <div class="item-price-toggler-content">
                <div class="prices-list pillow">
                    <span class="real_price current_price">
                        <?php echo price_with_currency( $item->price ); ?>
                    </span>
                </div>

                <div>
                    <button class="btn item-buy-btn pillow" type="button" data-behavior="add-item-to-cart"  data-cart-url="{!! route('cart.store', ['item_id' => $item->id]) !!}">В корзину</button>
                </div>

            </div>
        </form>

    </div>
</div>

