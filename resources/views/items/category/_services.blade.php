<div class="item-calc-container">
    <div class="item-calc">

        <div class="item-calc-standart">
            <label>
                <span class="label">Стандартные размеры:</span>
                <br/>
                <select>
                    <option value="">120x90</option>
                </select>
            </label>
        </div>

        <div class="item-calc-custom">
            <span class="label">Индивидуальные размеры:</span>
            <br/>
                            <span class="item-calc-w">
                                <label>
                                    <input type="text"/>
                                    <br/>
                                    ширина
                                </label>
                            </span>
                            <span class="item-calc-h">
                                <label>
                                    <input type="text"/>
                                    <br/>
                                    длина
                                </label>
                            </span>
        </div>

        <div class="item-calc-right-col">
            <div class="item-calc-price">
                                <span class="dashed">
                                    18 194 &#8376;
                                </span>
                                <span class="square-meter-price">
                                    7 980 &#8376;/м<sup><small>2</small></sup>
                                </span>
            </div>

            <button class="item-calc-btn" type="submit">Купить</button>
        </div>

    </div>
</div>

{{--<div class="related-items">--}}
    {{--<div class="related-items-title">--}}
        {{--Сопутствующие товары--}}
    {{--</div>--}}
    {{--<div class="related-item">--}}
        {{--<div class="related-item-img-container"></div>--}}
        {{--<div class="related-item-title">Подушка</div>--}}
        {{--<div class="related-item-price">5 000 &#8376;</div>--}}
    {{--</div>--}}
    {{--<div class="related-item">--}}
        {{--<div class="related-item-img-container"></div>--}}
        {{--<div class="related-item-title">Подушка</div>--}}
        {{--<div class="related-item-price">5 000 &#8376;</div>--}}
    {{--</div>--}}
    {{--<div class="related-item">--}}
        {{--<div class="related-item-img-container"></div>--}}
        {{--<div class="related-item-title">Подушка</div>--}}
        {{--<div class="related-item-price">5 000 &#8376;</div>--}}
    {{--</div>--}}
{{--</div>--}}