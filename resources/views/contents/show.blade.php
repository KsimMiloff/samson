@extends('layouts.site')

@include('partials._seotags', ['seotags' => $content->seotags])


@section('title')
    <div class="wavy-line wavy-bottom">
        <div class="container">
            <div class="pv-25">
                @include('partials.breadcrumbs', ['crumbs' => [
                    ['url' => '/', 'title' => 'Главная'],
                    ['url' => route('contents.index', ['category' => $content->category_id]), 'title' => $content->category->plural_title],
                    ['active' => true, 'title' => $content->title],
                ]])

                <h1 class="h1">
                    {!! $content->title !!}
                </h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="content-container">
        <div class="content pv-25 wysiwyg">
            {!! $content->desc !!}
        </div>

        @if ( count( $content->slider_ids ) )
            <div class="content pv-25 wysiwyg">
                <div class="fotorama" data-width="100%" data-ratio="3/2" data-autoplay="true">
                    @foreach( $content->slider_ids as $image_id)
                        {!! Html::picture($image_id, ['size' => '1000x345', 'crop' => false]) !!}
                    @endforeach
                </div>
            </div>
        @endif

    </div>
@stop