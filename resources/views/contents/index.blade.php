@extends('layouts.site')

<?php $seotags->title = $category->plural_title; ?>
@include('partials._seotags')

@section('title')
    <div class="wavy-line wavy-bottom">
        <div class="container">
            <div class="pv-25">
                @include('partials.breadcrumbs', ['crumbs' => [
                    ['url' => '/', 'title' => 'Главная'],
                    ['active' => true, 'title' => $category->plural_title],
                ]])

                <h4 class="h1">
                    {!! $category->plural_title !!}
                </h4>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="container">
        <div class="content pv-25">
            <div class="content-list">

                @forelse ($contents as $content)
                    <div class="content-el">
                        <h2 class="h3">
                            {!! link_to_route( 'contents.show', $content->title, ['category_id' => $content->category_id, 'id' => $content->seo_id] ) !!}
                        </h2>
                        <div class="wysiwyg">
                            {!! str_limit( strip_tags( $content->desc ), 300 ) !!}
                        </div>
                    </div>
                @empty
                    <i>{!! $category->plural_title !!} скоро будут ;)</i>
                @endforelse

            </div>
        </div>
    </div>

@stop

