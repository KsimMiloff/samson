@extends('layouts.site')

@include('partials._seotags')

@section('title')
    <div class="wavy-line wavy-bottom">
        <div class="container">
            <div class="pv-25">

                @include("partials._cart_status")

                @include('partials.breadcrumbs', ['crumbs' => [
                    ['url' => '/', 'title' => 'Главная'],
                    ['active' => true, 'title' => 'На заказ'],
                ]])

                <h1 class="h1">
                    Матрас по индивидуальному заказу
                </h1>

            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="container">

        <div class="pv-25">

            <div class="calc-container">
                <div class="calc">

                    {!! Form::open(['route' => ['cart.store', 'item_id' => 'custom_item'], 'id' => 'calculator-form']) !!}

                        <div class="calc-left-col">
                            <div class="calc-row" data-behavior='change-custom-square'>
                                <div class="calc-custom">
                                    <span class="label">Размеры:</span>
                                    <br/>
                                    <span class="calc-w">
                                        <label>
                                            {!! Form::number('width', $width, ['min' => 0, 'class' => 'small', 'data-behavior' => 'calculate-from-size']) !!}
                                            <br/>
                                            ширина
                                        </label>
                                    </span>
                                    <span class="calc-h">
                                        <label>
                                            {!! Form::number('height', $height, ['min' => 0, 'class' => 'small', 'data-behavior' => 'calculate-from-size']) !!}
                                            <br/>
                                            длина
                                        </label>
                                    </span>


                                </div>

                                <span class="calc-square-result">
                                    <span class="pre-value">~ </span>
                                    {!! Form::text('S', null, ['readonly', 'placeholder' => '?', 'onchange' => ""]) !!}
                                    <span>м<sup>2</sup></span>
                                </span>

                            </div>

                            <div class="calc-row align-center">

                                <div class="calc-price">
                                    <span class="dashed" style="display: nosne">
                                        {!! price_with_currency(0, 'dynamic-price price-value') !!}
                                    </span>
                                </div>

                                <br/>
                                <button class="btn calculator-btn" style="display: none" type="submit">В корзину</button>

                            </div>
                        </div>

                        <div class="calc-right-col">
                            <div class="calc-settings" data-behavior="calc-settings-listener">
                                <div class="calc-row">
                                    {!! var_title($calculator, 'spring_block') !!}
                                    {!! var_values($calculator, $item, 'spring_block') !!}
                                </div>

                                <div class="calc-row">
                                    {!! var_title($calculator, 'cover_1') !!}
                                    {!! var_values($calculator, $item, 'cover_1', ['only' => [ 'thermofelt', 'coir', 'latex', 'memorics', 'hollkon', 'spanbond', 'ppu_wabe' ]]) !!}
                                    {!! var_values($calculator, $item, 'cover_1', ['only' => [ 'ppu_1sm', 'ppu_2sm'], 'input_type' => 'radio' ]) !!}
                                </div>

                                <div class="calc-row">
                                    {!! var_title($calculator, 'cover_2') !!}
                                    {!! var_values($calculator, $item, 'cover_2', ['only' => [ 'thermofelt', 'coir', 'latex', 'memorics', 'hollkon', 'spanbond', 'ppu_wabe' ]]) !!}
                                    {!! var_values($calculator, $item, 'cover_2', ['only' => [ 'ppu_1sm', 'ppu_2sm' ], 'input_type' => 'radio']) !!}
                                </div>

                                <div class="calc-row">
                                    {!! var_title($calculator, 'upholstery') !!}
                                    {!! var_values($calculator, $item, 'upholstery') !!}
                                </div>

                                <div class="calc-row">
                                    {!! var_title($calculator, 'side_ppu') !!}
                                    {!! var_values($calculator, $item, 'side_ppu') !!}
                                </div>

                                <div class="calc-row">
                                    {!! var_values($calculator, $item, 'side_upholstery', ['input_type' => 'checkbox']) !!}
                                    {!! var_values($calculator, $item, 'spring_block_gain', ['var_name' => 'b', 'input_type' => 'checkbox']) !!}
                                </div>

                                {!! Form::hidden("K", $calculator->k) !!}

                            </div>
                        </div>

                    {!! Form::close() !!}

                </div>
            </div>

        </div>

    </div>

    <script>
        $(function() {
            // запрещаем форме отправляться по enter
            $('#calculator-form').on('keyup keypress', function(e) {
                var code = e.keyCode || e.which;
                if (code == 13) {
                    e.preventDefault();
                    return false;
                }
            });

        });
    </script>

@stop

