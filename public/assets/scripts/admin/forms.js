
make_thumb = function(file) {
    thumb = '' +
        '<div class="form-thumb">' +
        '<i class="glyphicon glyphicon-remove form-thumb-remove"  data-behavior="cancel-file"></i>' +
        '<div class="form-thumb-cell">' +
            '<div class="form-thumb-loader"><div class="spinner three-quarters-loader"></div></div>' +
            '<img src="' + file.preview + '">' +
        '</div>' +
        '</div>';


    $thumb = $(thumb);
    $thumb.data('file', file.data);

    return $thumb;
};


isNewFile = function(files, file) {
    var result = true;
    $.each(files, function( i, f ) {
        if (f.lastModified == file.lastModified) {
            result = false;
        }
    });
    return result;
};


remove_file = function($dialog, file) {
    files = $dialog.data('files');
    files = files.filter(function (f) {

        if (f.lastModified !== file.lastModified) {
            return true;
        }
    });
    $dialog.data('files', files);
};


store_file = function(file, $container) {
    url   = $container.data('url');
    token = $container.data('token');

    $thumb  = make_thumb(file);
    $loader = $thumb.find('.form-thumb-loader');
    $field  = $('<input type="hidden" name="' + $container.data('field_name') + '">');

    $thumb.append($field);
    $container.append($thumb);

    formData = new FormData();
    formData.append('file', file.data);
    formData.append('_token', token);

    xhr = $.ajax({
        url: url,
        type: 'post',
        data: formData,
        processData: false,
        contentType: false,
        $loader: $loader,
        $field: $field,
        $thumb: $thumb
    });

    xhr.done(function(data) {
        this.$field.val(data.file_id);
    });

    xhr.fail(function() {
        this.$thumb.addClass('error');
    });

    xhr.always(function() {
        this.$loader.hide();
        show_upload_message($container);
    });

};


show_upload_message = function($container) {
    $errors = $container.find('.error');

    if ($errors.length > 0) {
        msg = 'Не все файлы были загружены';
    } else {
        msg = '';//'Файлы загружены успешно';
    }

    $msg = $container.find('.upload_message');
    if ($msg.length == 0) {
        $msg = $('<div class="upload_message"></div>');
        $container.prepend($msg);
    }
    $msg.text(msg);
};


$(function(){

    $(document).on('click', '[data-behavior=file_dialog_btn]', function() {
        $dialog =  $(this).siblings('[data-behavior=file_dialog]');
        $dialog.click();
    });

    $(document).on('click', '[data-behavior=cancel-file]', function() {
        $thumb  = $(this).closest('.form-thumb');
        $dialog = $thumb.siblings('[data-behavior=file_dialog]');
        file    = $thumb.data('file');

        $dialog.data('button').removeClass('hide');
        $thumb.remove();
        remove_file($dialog, file);
    });

    $(document).on('change', "[data-behavior=file_dialog]", function() {
        $dialog    = $(this);
        $container = $dialog.data('container'); // внешняя оболочка виджета, в ней инкапсулированны все элементы
        inputFiles = $dialog.data('files'); // переменная в которой храним все файлы выбранные юзером

        //console.log($dialog);
        multiple = $container.data('multiple');

        if (inputFiles === undefined) {
            inputFiles = [];
        }

        if (multiple == false && inputFiles.length == 1) {
            return
        }

        // пробегаемся по юзерским файлам
        $.each($dialog[0].files, function( index, inputFile ) {

            // если в нашем списке есть еще не обработанный файл
            //if ( isNewFile(inputFiles, inputFile) ) {

                // готовимся читать его
                var reader = new FileReader();

                // успешно
                reader.onload = function (event) {
                    file = {}
                    file.preview = event.target.result;
                    file.data    = inputFile
                    store_file(file, $container);

                    inputFiles.push(inputFile); // запоминаем уже прочитанные файлы
                };
                // не успешно
                reader.onerror = function (event) {
                    alert("Файл загррузить не удалось");
                };

                reader.readAsDataURL(inputFile); // начинаем читать его

            //}
        });

        $dialog.val(''); // обнуляем значение поля, т.к. в действительночти не хоитм ничего посылать на сервер
        $dialog.data('files', inputFiles); // даем полю запомнить какие файлы через него загружали в прошлом

        if (multiple == false) {
            $dialog.data('button').addClass('hide');
        }

    });
});