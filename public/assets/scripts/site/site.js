collect_values_for_calculator = function()
{
    values = {};

    $required_fields = $('[data-calc-required=1]');
    values['required_is_checked'] = true;

    // список возможных переменных для формулы
    var_names  = ['b', 'l1', 'l2', 'm', 'd', 'g'];

    $.each( var_names, function( index, varname ) {
        // выбираем только обязательные переменные
        input_selector = '[name^=' + varname + '][data-calc-required=1]';
        $input = $(input_selector);

        // ещем хотябы одно обязательное и незаполненное поле
        if ($input.is('[data-calc-required=1]') && ! $input.is(':checked')) {
            values['required_is_checked'] = false; // ставим маркер обязательности как ложный
            return false; // break;
        }
    });

    $.each( var_names, function( index, varname ) {
        // теперь выбираем ВСЕ переменные
        input_selector = '[name^=' + varname + ']';
        $input = $(input_selector);

        values[varname] = 0;

        // если переменная выбранна, то сохраняем
        if ( $input.is( ':checked' ) )
        {
            $.each( $( input_selector + ':checked' ), function( index, item ) {
                $item = $(item);
                values[varname] += $item.data('calc-weight')
            });
        }

    });


    $width  = $( document ).find( '[data-behavior=calculate-from-size][name="width"]' );
    $height = $( document ).find( '[data-behavior=calculate-from-size][name="height"]' );

    values['s'] = calculate_square( $width.val(), $height.val() );
    values['p'] = calculate_perimeter( $width.val(), $height.val() );
    values['k'] = $('[name=K]').val();

    return values;

};

calculate_price = function(values) {

    if ( values['required_is_checked']
        && values['s']
        && values['k'])
    {
        sum_vars_to_square = ( values['b'] + values['l1'] + values['l2'] + values['m'] + values['g'] );
        price = ( sum_vars_to_square * values['s'] + values['d'] * values['p'] ) * values['k'];

        $('.dynamic-price.price-value').text( price.toFixed(0) );
        $('.calculator-btn').show();
    } else {
        $('.calculator-btn').hide();
    }
};



calculate_square = function(w, h) {
    square = 0;
    if (w > 0 && h > 0) {
        square = ( w * h * 0.0001).toFixed(2);
    }
    return square;
};

calculate_perimeter = function(w, h) {
    perimeter = 0;
    if (w > 0 && h > 0) {
        perimeter = w * 2 + h *2;
    }
    return perimeter;
};


$(function() {

    calculate_price( collect_values_for_calculator() );

    $( document ).on('change', '[data-behavior=change-square]', function()
    {
        selected = $(this).val();
        $real_price      = $(this).closest('.dynamic-price').find('.real_price');
        $dicounted_price = $(this).closest('.dynamic-price').find('.dicounted_price');

        if ( $real_price.length != 0 )
        {
            $real_price.find('.price-value')
                .html( $real_price.data( 'values' )[selected] );
        }

        if ( $dicounted_price.length != 0 )
        {
            $dicounted_price.find('.price-value')
                .html( $dicounted_price.data( 'values' )[selected] );
        }

    });

    $( document ).on('change keyup', '[data-behavior=individulize_size]', function()
    {
        $width_field  = $( '[data-behavior=individulize_size][name*=width]' );
        $height_field = $( '[data-behavior=individulize_size][name*=height]' );

        $real_price      = $(this).closest('.manual-dynamic-price').find('.real_price');
        $dicounted_price = $(this).closest('.manual-dynamic-price').find('.dicounted_price');


        if ($width_field.val() && $height_field.val())
        {
            square = $width_field.val() * $height_field.val() * 0.0001;

            if ( $real_price.length != 0 )
            {
                number = $real_price.data( 'price-per-square-meter' ) * square;
                number = number.toLocaleString('ru-RU', { maximumFractionDigits: 0 });

                $real_price.find('.price-value')
                    .html( number );

                //if ( number < 1 )
                //{
                //    $('[data-behavior=add-item-to-cart]').hide();
                //} else {
                //    $('[data-behavior=add-item-to-cart]').show();
                //}
            }

            if ( $dicounted_price.length != 0 )
            {
                number = $dicounted_price.data( 'price-per-square-meter' ) * square;
                number = number.toLocaleString('ru-RU', { maximumFractionDigits: 0 });

                $dicounted_price.find('.price-value')
                    .html( number );
            }
        }


    });


    $square_selects = $( document ).find('[data-behavior=change-square]');
    if ( $square_selects.length != 0 )
    {
        $square_selects.change();

    }


    $( document ).on('keyup change', '[data-behavior=customize-size]', function()
    {
        $width  = $( document ).find( '[data-behavior=customize-size][name="width"]' );
        $height = $( document ).find( '[data-behavior=customize-size][name="height"]' );

        is_valid_size = $width.val() * $height.val();

        $cart_btn = $('[data-behavior=add-item-to-cart]');
        $calc_btn = $('[data-behavior=calculate-custom-item]');

        if ($cart_btn.length > 0 && $calc_btn.length > 0) {
            if (is_valid_size) {
                $cart_btn.hide();
                $calc_btn.show();
            } else {
                $cart_btn.show();
                $calc_btn.hide();
            }
        }
    });


    $( document ).on('keyup change', '[data-behavior=calculate-from-size]', function()
    {
        values = collect_values_for_calculator();

        $s = $('[data-behavior=change-custom-square] [name=S]');
        $s.val( values['s'] );
        $s.width(( values['s'].length + 1 ) * 11);

        calculate_price( values );
    });



    $(document).on('click', '[data-behavior=add-item-to-cart]', function() {
        url = $(this).data('cart-url');
        data = $(this).closest('form').serialize();

        var jqxhr = $.post( url, data)
            .done(function(html) {
                $('.cart-status').replaceWith(html);
            })
            .fail(function() {
                alert( "Ой! добавить товар в корзину не удалось!" );
            })
            .always(function() {
            });
    });


    //
    //$(document).on('click', '[data-calc-required=0], [data-calc-required=1]', function(e) {
    //    previousValue = $(this).data('previousValue');
    //
    //    if (previousValue) {
    //        $(this).prop('checked', false);
    //    } else {
    //        $(this).prop('checked', true);
    //    }
    //
    //    $(this).data('previousValue', $(this).prop('checked'));
    //});


    $(document).on('click', '[value=thermofelt]', function(e) {
        same_value = $(this).prop('checked');
        $('[value=thermofelt]').prop('checked', same_value);
    });



    $(document).on('click', '[data-behavior=calc-settings-listener] input', function() {
        calculate_price( collect_values_for_calculator() );
    });



    $(document).on('click', '[data-behavior=buy]', function() {
        href = $(this).data('target');
        $.fancybox({
            href        : href,
            maxWidth	: 500,
            maxHeight	: 600,
            fitToView	: false,
            width		: '70%',
            height		: '70%',
            autoSize	: false,
            closeClick	: false,
            openEffect	: 'none',
            closeEffect	: 'none'
        });
    });


    $(document).on('click', '[data-behavior=fancybox-link]', function(e) {
        e.preventDefault();
        href = $(this).attr('href');

        $.fancybox({
            href: href,
            type: "image",
            authoWidth: true
        });
    });

});
